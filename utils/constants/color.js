import ColorModel from 'coeur/models/color';

/*
 * 08 is .03
 * 30 is .1
 * 33 is .2
 * 66 is .4
 * 80 is .5
 * 99 is .6
 * B3 is .7
 * CC is .8
 * E5 is .9
 */

const White = new ColorModel([
		'FAFAFA', // WHITE
		'FFFFFF', // WHITE TWO
	], 2)
	, Black = new ColorModel([
		'27272b',
	], 1)
	, Pink = new ColorModel([
		'EC008C',
	], 1)
	, Red = new ColorModel([
		'DB4F31', // PALE RED
		'D95038', // PALE RED TWO
		'D85137', // PALE RED THREE
	], 1)
	, Orange = new ColorModel([
		'FB7538',
		'FBB232',
	], 1)
	, Blue = new ColorModel([
		'2E3192', // #2E3192
	], 1)
	, Yellow = new ColorModel([
		'FBB232', // #2E3192
	], 1)
	, Green = new ColorModel([
		'477E54', // LIGHT FOREST GREEN
	], 1)
	, Grey = new ColorModel([
		'E2E9E7', // LIGHT GREY
		'E9EDF8', // PALE GREY
	], 1)

const COLORS = {

	primary: '#2E3192',

	transparent: 'transparent',

	white: White,
	black: Black,
	orange: Orange,
	// purple: Purple,
	pink: Pink,
	blue: Blue,
	green: Green,
	yellow: Yellow,
	red: Red,
	grey: Grey,

	default: new ColorModel([
		'2E3192',	// PRIMARY	'#986C79'
		'FFFFFF',	// WHITE	'#FFFFFF'
		'0F0F14',	// BLACK	'#0F0F14'
		'EBECF0',	// GREY		'#EBECF0'
		'C1C7D0',	// BORDER	'#C1C7D0'
	]),
};


export default COLORS
