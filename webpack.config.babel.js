import Package from './package.json'
import path from 'path'
import fs from 'fs'
import webpack from 'webpack'
import CopyWebpackPlugin from 'copy-webpack-plugin'
// import UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import TerserPlugin from'terser-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import BrowserSyncPlugin from 'browser-sync-webpack-plugin'
import DotEnv from 'dotenv'

console.log(`Running ${Package.name}`)

DotEnv.config()

const envValues = DotEnv.parse(fs.readFileSync('.env'))

const isDevelopment = process.env.ENV === 'local'
	, plugins = [
		new webpack.DefinePlugin({
			'process.env': JSON.stringify(envValues),
		}),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'template/view.pug',
			chunks: ['index'],
			// inject: false,
			hash: true,
		}),
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: 'assets/css/[name].css',
			// filename: '[name].[hash].css',
			// chunkFilename: '[id].[hash].css',
		}),
	]

if(isDevelopment) {
	plugins.push(new webpack.HotModuleReplacementPlugin())
	plugins.push(new BrowserSyncPlugin(
		// BrowserSync options
		{
			// browse to http://localhost:3000/ during development
			host: 'localhost',
			port: process.env.PORT,
			// proxy the Webpack Dev Server endpoint
			// (which should be serving on http://localhost:3100/)
			// through BrowserSync
			proxy: `http://localhost:${process.env.BROWSERSYNC_PORT}/`,
		},
		// plugin options
		{
			// prevent BrowserSync from reloading the page
			// and let Webpack Dev Server take care of this
			reload: false,
		}
	))
} else {
	plugins.unshift(new CopyWebpackPlugin([{
		from: 'assets/static',
		to: 'public/../',
		// context: 'assets/root/',
	}]))
}

export default {
	mode: isDevelopment ? 'development' : 'production',
	entry: {
		index: './index.js',
	},
	resolve: {
		// root: __dirname,
		alias: {
			// '': __dirname,
			[`@${Package.name}`]: __dirname,
		},
		extensions: ['*', '.mjs', '.js', '.jsx'],
	},
	output: {
		filename: 'assets/js/[name].[hash].js',
		path: path.resolve(__dirname, 'public'),
		publicPath: isDevelopment ? '/' : './', // can point to another link nih
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				styles: {
					name: 'styles',
					test: /\.css$/,
					chunks: 'all',
					enforce: true,
				},
			},
		},
		minimizer: [
			new TerserPlugin({
				parallel: true,
				terserOptions: {
					ecma: 6,
				},
			}),
			// new UglifyJsPlugin({
			// 	cache: true,
			// 	parallel: true,
			// 	sourceMap: !!isDevelopment, // set to true if you want JS source maps
			// }),
			new OptimizeCSSAssetsPlugin({}),
		],
	},
	module: {
		rules: [{
			test: /\.(scss|css)$/,
			use: [{
				loader: isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
				options: isDevelopment ? undefined : {
					outputPath: 'assets/css/',
					publicPath: '../../',
				},
			}, {
				loader: 'css-loader',
				options: {
					// root: path.resolve(__dirname, 'assets/'),
					// minimize: true,
					// modules: true,
					sourceMap: process.env.DEBUG === 'true',
					// importLoaders: 2,
				},
			}, {
				loader: 'postcss-loader',
				options: {
					plugins: [],
				},
			}, {
				loader: 'sass-loader',
				options: {
					includePaths: [
						'assets/scss/',
						'node_modules/',
						'modules/',
					],
				},
			}],
		}, {
			test: /\.jsx?$/,
			exclude: /node_modules\/(?!(coeur)\/).*/,
			loader: 'babel-loader',
			options: {
				babelrcRoots: [
					'.',
					'../',
				],
			},
		}, {
			test: /\.pug$/,
			use: [{
				loader: 'html-loader',
				options: {
					// interpolate: true,
				},
			}, {
				loader: 'pug-html-loader',
				options: {
					basedir:  path.resolve(__dirname, 'modules/'),
				},
			}],
		}, {
			test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
			use: {
				loader: 'url-loader',
				options: {
					// Limit at 50k. Above that it emits separate files
					limit: 100,
					// url-loader sets mimetype if it's passed.
					// Without this it derives it from the file extension
					mimetype: 'application/font-woff',
					// Output below fonts directory
					name: '[name].[ext]',
					useRelativePath: !isDevelopment,
					outputPath: 'assets/font/',
				},
			},
		}, {
			test: /\.(png|jpg|gif)$/,
			use: {
				loader: 'url-loader',
				options: {
					// Limit at 50k. Above that it emits separate files
					limit: 100,
					// url-loader sets mimetype if it's passed.
					// Without this it derives it from the file extension
					name: '[path]/[name].[ext]',
					useRelativePath: !isDevelopment,
					outputPath: 'assets/',
					// outputPath: 'assets/img/',
					context: 'assets/js/',
				},
			},
		}],
	},
	plugins,
	devtool: 'source-map',
	devServer:{
		contentBase: [
			'assets',
		],
		before(app) {
			app.use('/', function(req, res, next) {
				const options = {
					root: __dirname + '/assets/static',
					dotfiles: 'deny',
					headers: {
						'x-timestamp': Date.now(),
						'x-sent': true,
					},
				};

				const fileName = req.path;

				res.sendFile(fileName, options, function(err) {
					if (err) {
						next();
					}
				});
			})
		},
		inline: true,
		hot: true,
		port: process.env.BROWSERSYNC_PORT,
		watchContentBase: true,
	},
};
