import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

const margin = Sizes.app.width <= 414 ? Sizes.fluid(24, 414) : Sizes.fluid(50)

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.white.primary,
		marginLeft: '7%',
		marginRight: '7%',
		marginTop: '16%',
		marginBottom: '16%',
		padding: '10%',
	},

	italic: {
		marginTop: Sizes.fluid(20),
	},

	price: {
		paddingTop: Sizes.app.width <= 414
			? Sizes.fluid(24, 414)
			: Sizes.fluid(70),
	},
	data: {
		paddingTop: Sizes.app.width <= 414
			? Sizes.fluid(24, 414)
			: Sizes.fluid(100),
	},
	default: {
		paddingBottom: 16,
	},
	thick: {
		paddingBottom: Sizes.app.width <= 414 ? 16 : Sizes.fluid(30),
	},

	bold: {
		fontWeight: '800',
	},

	input: {
		width: `calc(100% - ${margin}px)`,
		marginLeft: margin,
		marginRight: margin,
	},

	button: {
		marginTop: Sizes.app.width <= 414 ? 16 : Sizes.fluid(60),
	},

	total: {
		marginTop: Sizes.fluid(70),
	},

	imageTitle: {
		alignSelf: 'center',
		// eslint-disable-next-line no-nested-ternary
		height: Sizes.app.width === 1280
			? 90
			// eslint-disable-next-line no-nested-ternary
			: Sizes.app.width === 1024
				? 80
				: Sizes.app.width <= 414
					? Sizes.fluid(200)
					: Sizes.fluid(100),
		marginBottom: Sizes.fluid(200),
		zIndex: 2,
	},

	imageTriangleTop: {
		position: 'absolute',
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 963 * 1.5 : 963),
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 444 * 1.5 : 444),
		right: Sizes.fluid(-60),
	},
	imageTriangleBottom: {
		position: 'absolute',
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 963 * 1.5 : 963),
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 444 * 1.5 : 444),
		left: Sizes.fluid(-200),
		bottom: Sizes.fluid(-100),
		zIndex: -1,
	},
	imageCircleTop: {
		position: 'absolute',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 756 * 1.5 : 756),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 623 * 1.5 : 623),
		left: Sizes.fluid(-300),
		top: Sizes.fluid(40),
	},
	imageCircleBottom: {
		position: 'absolute',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 756 * 1.5 : 756),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 623 * 1.5 : 623),
		right: Sizes.fluid(-250),
		bottom: Sizes.fluid(70),
	},

	'@media (max-width: 1400px)': {
		titleImage: {
			alignSelf: 'center',
			height: 100,
			marginBottom: 80,
		},
		container: {
			zIndex: 1,
		},
	},
	'@media (max-width: 1264px)': {
		titleImage: {
			height: 90,
			marginBottom: 50,
		},
	},
	'@media (max-width: 768px)': {
		titleImage: {
			height: 80,
			marginBottom: 50,
		},
	},
	'@media (max-width: 414px)': {
		container: {
			paddingLeft: Sizes.margin.thick,
			paddingRight: Sizes.margin.thick,
			paddingTop: 32,
			paddingBottom: 32,
		},
		titleImage: {
			width: 280,
			height: 80,
			marginBottom: 32,
		},
	},
})
