import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import FormatHelper from 'coeur/helpers/format';

import Sizes from 'coeur/constants/size';

import PaymentService from 'app/services/payment';

import BaskervilleBit from 'modules/bits/baskerville';
import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
// import InputFileBit from 'modules/bits/input.file';
import InputValidatedBit from 'modules/bits/input.validated';
import PageBit from 'modules/bits/page';
// import TextBit from 'modules/bits/text';
// import TextInputBit from 'modules/bits/text.input';
// import TouchableBit from 'modules/bits/touchable';

import AmountLego from 'modules/legos/amount';

import Styles from './style';

const Background = BoxImageBit.resolve('registration/background.jpg')
const ElementTriangle = ImageBit.resolve('registration/element-triangel.png')
const ElementCircleM = ImageBit.resolve('registration/element-circle.png')
const RegistrationTitle = ImageBit.resolve('registration/regis.png')
const ElementCircle = ImageBit.resolve('element-2.png')


export default ConnectHelper(
	class RegistrationPage extends PageModel {

		static routeName = 'registration'

		constructor(p) {
			super(p, {
				isValid: false,
				isSending: false,
				price: 105000,
				amount: 1,
				total: 105000,
				data: {},
			})

			this._data = [{
				no: '1. ',
				id: 'name',
				type: InputValidatedBit.TYPES.NAME,
				title: 'Nama Lengkap',
			}, {
				no: '2. ',
				id: 'age',
				type: InputValidatedBit.TYPES.INPUT,
				title: 'Usia',
			}, {
				no: '3. ',
				id: 'handphone',
				type: InputValidatedBit.TYPES.PHONE,
				title: 'Nomor HP yang dapat dihubungi',
			}, {
				no: '4. ',
				id: 'email',
				type: InputValidatedBit.TYPES.EMAIL,
				title: 'Alamat Email',
			}, {
				no: '5. ',
				id: 'city',
				type: InputValidatedBit.TYPES.INPUT,
				title: 'Kota Domisili',
			}, {
				no: '6. ',
				id: 'church',
				type: InputValidatedBit.TYPES.INPUT,
				title: 'Nama Gereja/Organisasi',
			}]
		}

		onChangeAmount = amount => {
			this.setState({
				amount,
				total: amount * this.state.price,
			})
		}

		onChange = (key, value) => {
			this.state.data[key] = value

			// check completion
			if(
				this.state.data.name &&
				this.state.data.age &&
				this.state.data.handphone &&
				this.state.data.email &&
				this.state.data.city &&
				this.state.data.church
			) {
				this.setState({
					isValid: true,
				})
			} else {
				this.setState({
					isValid: false,
				})
			}
		}

		onPayment = () => {
			this.setState({
				isSending: true,
			}, () => {
				const names = this.state.data.name.split(' ')
				PaymentService.order({
					type: 'thethird',
					customer: {
						first_name: names.shift(),
						last_name: names.join(' '),
						email: this.state.data.email,
					},
					products: [{
						id: 'thethird_ticket',
						name: 'The Third — Entrance Ticket',
						price: this.state.price,
						quantity: this.state.amount,
						metadata: this.state.data,
					}],
				}).then(link => {
					window.location.href = link
				})
			})
		}

		dataRenderer = ({title, no, id, type}) => {
			return (
				<BoxBit key={ id } unflex style={Styles.thick}>
					<BaskervilleBit
						type={BaskervilleBit.TYPES.REGIS_PARAGRAPH}
					>
						{ no } { title }
					</BaskervilleBit>
					<InputValidatedBit
						iconSize={ Sizes.app.width <= 414 ? Sizes.fluid(24, 414) : Sizes.fluid(48) }
						type={ type }
						onChange={ this.onChange.bind(this, id) }
						style={Styles.input}
					/>
				</BoxBit>
			)
		}

		// inputFileRenderer() {
		// 	return (
		// 		<TouchableBit unflex style={Styles.inputFile}>
		// 			<BaskervilleBit
		// 				type={BaskervilleBit.TYPES.REGIS_PARAGRAPH}
		// 			>
		// 				<TextBit style={Styles.bold}>Choose file</TextBit> or <TextBit style={Styles.bold}>drag here</TextBit>{'\n'}Size limit: 10 MB
		// 			</BaskervilleBit>
		// 		</TouchableBit>
		// 	)
		// }

		render() {
			return super.render(
				<PageBit>
					<BoxImageBit source={ Background } >
						<BoxBit style={Styles.container}>
							<ImageBit source={ RegistrationTitle } style={ Styles.imageTitle } />
							<BaskervilleBit
								type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH }
							>
								Untuk menyelesaikan pendaftaran ini harap menyelesaikan pembayaran dalam 1 x 24 jam.
							</BaskervilleBit>
							<BaskervilleBit italic
								type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH_2 }
								style={Styles.italic}
							>
								To complete this registration, please kindly make the payment in 1 x 24 hours.
							</BaskervilleBit>
							<BoxBit unflex
								style={Styles.price}
							>
								<BaskervilleBit
									type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH }
								>
									Ticket Price:
								</BaskervilleBit>
								<BaskervilleBit
									type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH }
								>
									IDR 105.000 / Person
								</BaskervilleBit>
							</BoxBit>

							<BoxBit unflex style={Styles.price}>
								<BaskervilleBit
									type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH }
								>
									Berapa banyak tiket yang akan anda pesan ?
								</BaskervilleBit>
								<AmountLego value={ this.state.amount } onChange={ this.onChangeAmount } />
								<BaskervilleBit
									type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH }
									weight="bold"
									style={ Styles.total }
								>
									Total: Rp { FormatHelper.currency( this.state.total ) }
								</BaskervilleBit>
							</BoxBit>
							<BoxBit style={Styles.data}>
								<BaskervilleBit weight="bold"
									type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH }
								>
									Data
								</BaskervilleBit>
								<BaskervilleBit
									type={ BaskervilleBit.TYPES.REGIS_PARAGRAPH }
									style={Styles.default}
								>
									Silahkan isi data dibawah sesuai data pemesan ( satu orang sebagai perwakilan).
								</BaskervilleBit>
								{ this._data.map(this.dataRenderer) }
							</BoxBit>
							<ButtonBit
								title="Submit"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								// eslint-disable-next-line no-nested-ternary
								state={ this.state.isValid ? this.state.isSending ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								onPress={ this.onPayment }
								style={[ Styles.input, Styles.button ]}
							/>
						</BoxBit>
						<ImageBit source={ ElementTriangle } style={ Styles.imageTriangleTop } />
						<ImageBit source={ ElementCircleM } style={ Styles.imageCircleTop } />
						<ImageBit source={ ElementCircle } style={ Styles.imageCircleBottom } />
						<ImageBit source={ ElementTriangle } style={ Styles.imageTriangleBottom } />
					</BoxImageBit>
				</PageBit>
			)
		}
	}
)
