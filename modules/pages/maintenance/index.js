import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MaintenancePagelet from 'modules/pagelets/maintenance'


export default ConnectHelper(
	class MaintenancePage extends PageModel {

		static routeName = 'maintenance'

		render() {
			return super.render(
				<MaintenancePagelet />
			)
		}
	}
)
