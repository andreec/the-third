import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		paddingTop: 48,
		paddingLeft: Sizes.fluid(240),
		paddingRight: Sizes.fluid(240),
		paddingBottom: Sizes.fluid(240),
	},

	background: {
		zIndex: 1,
	},

	schedule: {
		color: Colors.blue.primary,
		paddingBottom: 48,
	},

	title: {
		marginBottom: Sizes.app.width <= 414 ? 90 : 157,
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 433 * 1.5 : 433),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 136 * 1.5 : 136),
		alignSelf: 'center',
	},
	white: {
		color: Colors.white.primary,
	},

	box: {
		backgroundColor: Colors.black.primary,
		width: Sizes.app.width <= 1024
			? Sizes.fluid(470, 1024)
			: Sizes.fluid(470),
		height: Sizes.app.width <= 1024
			? Sizes.fluid(650, 1024)
			: Sizes.fluid(650),
		marginTop: Sizes.app.width <= 414 ? 55 : 105,
		alignSelf: 'center',
		marginBottom: Sizes.app.width <= 414 ? 80 : 130,
	},
	boxText: {
		color: Colors.white.primary,
		alignSelf: 'center',
	},
	pink: {
		backgroundColor: Colors.pink.primary,
		justifyContent: 'center',
		flexGrow: 3,
	},
	blue: {
		backgroundColor: Colors.blue.primary,
		justifyContent: 'center',
		flexGrow: 2,
	},
	black: {
		flexGrow: 2,
		justifyContent: 'center',
	},

	session: {
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 1093 * 1.5 : 1093),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 1373 * 1.5 : 1373),
	},

	element1: {
		position: 'absolute',
		width: Sizes.fluid(1777),
		height: Sizes.fluid(774),
		top: '18%',
		alignSelf: 'center',
	},
	element2: {
		position: 'absolute',
		width: Sizes.fluid(482),
		height: Sizes.fluid(482),
		right: Sizes.fluid(-200),
		top: Sizes.fluid(200),
	},
	elementCircle: {
		position: 'absolute',
		width: Sizes.fluid(756),
		height: Sizes.fluid(623),
		left: Sizes.fluid(-250),
		top: Sizes.fluid(260),
	},
	elementCircle2: {
		position: 'absolute',
		width: Sizes.fluid(756),
		height: Sizes.fluid(623),
		left: Sizes.fluid(-270),
		bottom: Sizes.fluid(690),
	},
	elementSquare: {
		position: 'absolute',
		width: Sizes.fluid(756),
		height: Sizes.fluid(623),
		right: Sizes.fluid(-50),
		bottom: Sizes.fluid(210),
	},
	elementRegistration: {
		position: 'absolute',
		width: Sizes.fluid(1734),
		height: Sizes.fluid(1527),
		bottom: 0,
		alignSelf: 'center',
	},

	'@media (max-width: 414px)': {
		container: {
			paddingTop: 48,
			paddingLeft: Sizes.margin.thick,
			paddingRight: Sizes.margin.thick,
			paddingBottom: 48,
		},
		box: {
			width: Sizes.fluid(300, 375),
			height: Sizes.fluid(420, 375),
			marginTop: 32,
			alignSelf: 'center',
			marginBottom: Sizes.app.width <= 414 ? 80 : 130,
		},

		session: {
			width: Sizes.fluid(640 * 2.5),
			height: Sizes.fluid(1140 * 2.5),
		},
	},
})
