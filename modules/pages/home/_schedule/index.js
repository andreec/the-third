import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Sizes from 'coeur/constants/size';

import AxisBit from 'modules/bits/axis';
import AlvaroBit from 'modules/bits/alvaro';
import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import ImageBit from 'modules/bits/image';

import ScheduleLego from 'modules/legos/schedule';

import RegistrationPart from '../_register';

import Styles from './style';

const BackgroundSchedule = BoxImageBit.resolve('schedule/schedule.jpg')
const Element1 = ImageBit.resolve('schedule/element-1.png')
const Element2 = ImageBit.resolve('schedule/element-2.png')
const ElementCircle = ImageBit.resolve('element-2.png')
const ElementSquare = ImageBit.resolve('element-1.png')
const ElementRegistration = ImageBit.resolve('element-registration.png')


export default ConnectHelper(
	class SchedulePart extends StatefulModel {

		constructor(p) {
			super(p, {
			}, [
			])
		}

		render() {
			return (
				<BoxImageBit
					source={ BackgroundSchedule }
					resizeMode={BoxImageBit.TYPES.COVER}
					style={Styles.background}
				>
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ Element1 }
							style={Styles.element1}
						/>
					) }
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ Element2 }
							style={Styles.element2}
						/>
					) }
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ ElementCircle }
							style={Styles.elementCircle}
						/>
					) }
					<BoxBit style={Styles.container}>
						<AxisBit
							type={AxisBit.TYPES.HERO}
							align="center"
							style={Styles.schedule}
						>
							SCHEDULE
						</AxisBit>
						<AlvaroBit
							type={AlvaroBit.TYPES.HEADER_2}
							style={Styles.white}
							weight="bold"
						>
							50 DAYS MOVEMENT
						</AlvaroBit>
						<BoxBit unflex style={Styles.box}>
							<BoxBit row style={{flexGrow: .8}}>
								<BoxBit style={Styles.pink}>
									<AxisBit
										type={AxisBit.TYPES.HEADER_1}
										align="center"
										style={Styles.boxText}
									>
										NOV
									</AxisBit>
								</BoxBit>
								<BoxBit style={Styles.blue}>
									<AxisBit
										type={AxisBit.TYPES.HEADER_1}
										align="center"
										style={Styles.boxText}
									>
										1
									</AxisBit>
								</BoxBit>
							</BoxBit>
							<BoxBit style={Styles.black}>
								<AxisBit
									type={AxisBit.TYPES.HEADER_1}
									align="center"
									style={Styles.boxText}
								>
									DOA KELILING
								</AxisBit>
							</BoxBit>
						</BoxBit>
						<AlvaroBit
							type={AlvaroBit.TYPES.HEADER_2}
							style={Styles.white}
							weight="bold"
						>
							SESSION
						</AlvaroBit>
						{/* <ImageBit
							source={ Sizes.app.width <= 414
								? SessionScheduleMobile
								: SessionSchedule }
							style={Styles.session}
						/> */}
						{ Sizes.app.width <= 414 ? undefined : (
							<ImageBit
								source={ ElementRegistration }
								style={Styles.elementRegistration}
							/>
						) }
						<ScheduleLego />
						<RegistrationPart />
					</BoxBit>
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ ElementSquare }
							style={Styles.elementSquare}
						/>
					) }
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ ElementCircle }
							style={Styles.elementCircle2}
						/>
					) }
				</BoxImageBit>
			)
		}
	}
)
