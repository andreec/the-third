import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import Sizes from 'coeur/constants/size';

import AlvaroBit from 'modules/bits/axis';
import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import PageBit from 'modules/bits/page';

import FooterLego from 'modules/legos/footer';
import HeaderNavigationLego from 'modules/legos/header.navigation';

import SchedulePart from './_schedule';
import SpeakersPart from './_speakers';
import VisionPart from './_vision';

import Styles from './style';

const Background = BoxImageBit.resolve('background-landing.jpg')
const BackgroundMobile = BoxImageBit.resolve('background-landing-mobile.jpg')
const TheThird = ImageBit.resolve('the-third-yellow.png')
const TheThirdMobile = ImageBit.resolve('the-third-yellow-mobile.png')

const ConferenceLogotype = ImageBit.resolve('conference-logotype.png')
const ConferenceLogotypeMobile = ImageBit.resolve('conference-logotype-mobile.png')

const LogoBlack = ImageBit.resolve('gdnablack.png')
const Element = ImageBit.resolve('element-homepage.png')

const Date = ImageBit.resolve('date.png')
const DateMobile = ImageBit.resolve('date-mobile.png')


export default ConnectHelper(
	class HomePage extends PageModel {

		static routeName = 'home'

		constructor(p) {
			super(p, {
			}, [
				'onNavigateToRegister',
			])
		}

		onNavigateToRegister() {
			this.navigator.navigate('registration')
		}

		render() {
			return super.render(
				<PageBit>
					<BoxImageBit
						source={ Sizes.app.width <= 414 ? BackgroundMobile : Background }
						style={Styles.background}
					>
						{ Sizes.app.width <= 414 ? undefined : (
							<ImageBit
								source={ Element }
								style={Styles.element}
							/>
						) }
						<BoxBit unflex>
							<ImageBit
								source={ LogoBlack }
								style={Styles.logoBlack}
							/>
							<BoxBit style={Styles.containerImage}>
								<ImageBit
									source={ Sizes.app.width <= 414
										? ConferenceLogotypeMobile
										: ConferenceLogotype }
									style={Styles.logoType}
								/>
								<ImageBit
									source={ Sizes.app.width <= 414
										? TheThirdMobile
										: TheThird }
									style={Styles.theThird}
								/>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex>
							<ImageBit
								source={ Sizes.app.width <= 414
									? DateMobile
									: Date }
								style={Styles.date}
							/>
							<ButtonBit
								title="BUY TICKETS"
								width={ButtonBit.TYPES.WIDTHS.FIT}
								size={ButtonBit.TYPES.SIZES.TINY}
								style={Styles.buyTicket}
								onPress={this.onNavigateToRegister}
							/>
						</BoxBit>
					</BoxImageBit>
					{ Sizes.app.width <= 414 ? undefined : (
						<HeaderNavigationLego />
					) }
					<VisionPart />
					<SpeakersPart />
					<SchedulePart />
					{/* <RegistrationPart /> */}
					<FooterLego />
				</PageBit>
			)
		}
	}
)
