import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
// import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import BaskervilleBit from 'modules/bits/baskerville';
// import PageBit from 'modules/bits/page';
import ShadowBit from 'modules/bits/shadow';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

const Background = BoxImageBit.resolve('vision/background-vision.jpg')
const BackgroundMobile = BoxImageBit.resolve('vision/background-vision-mobile.jpg')
const Element2 = ImageBit.resolve('element-2.png')


export default ConnectHelper(
	class VisionPart extends StatefulModel {

		constructor(p) {
			super(p, {
			}, [
			])
		}

		render() {
			return (
				<BoxImageBit
					source={ Background }
				>
					{ Sizes.app.width <= 414 ? undefined : (
						<BoxImageBit
							source={ Element2 }
							style={Styles.element1}
						/>
					) }
					<BoxBit style={Styles.container}>
						<BaskervilleBit
							type={BaskervilleBit.TYPES.HEADER_1}
							align="center"
							style={Styles.ayat}
						>
						“Ketika tiba hari Pentakosta, semua orang percaya berkumpul di satu tempat. Tiba-tiba turunlah dari langit suatu bunyi seperti tiupan angin keras yang memenuhi seluruh rumah...{'\n'}Maka penuhlah mereka dengan Roh Kudus…”{'\n'}Kis 2:1-4
						</BaskervilleBit>
						<BaskervilleBit
							type={BaskervilleBit.TYPES.HEADER_2}
							weight="normal"
							align="center"
							style={Styles.vision}
						>
							Pada pentakosta yang pertama, kurang lebih 3000 jiwa dibaptis dan menyerahkan diri mereka pada Kristus. Sejak saat itu gelombang pertobatan terus bergerak meluas. Pada awal abad ke-20 terjadi lagi kebangkitan rohani yang luar biasa di Azusa Street (Los Angeles) yang dipercaya sebagai Pentakosta kedua. Saat itu William Seymour bernubuat bahwa 100 tahun ke depan Roh Kudus akan dicurahkan jauh lebih besar, lebih dahsyat, dan lebih hebat lagi. Melalui visi yang ditaruh dalam hati hamba Tuhan Pdt. Dr. Ir. Niko Njotorahardjo, kami percaya bahwa inilah saatnya api pentakosta ketiga menyala. Seperti murid-murid di hari Pentakosta pertama, demikian juga kita di Pentakosta ketiga ini akan dipakai Tuhan menjadi saksi-saksi Kristus di jaman ini.
						</BaskervilleBit>
					</BoxBit>
				</BoxImageBit>
			)
		}
	}
)
