import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		paddingTop: Sizes.fluid(104),
		paddingLeft: Sizes.fluid(240),
		paddingRight: Sizes.fluid(240),
		paddingBottom: Sizes.fluid(124),
	},

	ayat: {
		color: Colors.white.primary,
		paddingBottom: Sizes.fluid(64),
	},

	vision: {
		color: Colors.white.primary,
	},

	element1: {
		position: 'absolute',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 756 * 1.5 : 756),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 623 * 1.5 : 623),
		right: Sizes.fluid(-230),
		top: Sizes.fluid(340),
	},

	'@media (max-width: 414px)': {
		container: {
			paddingTop: Sizes.fluid(48, 375),
			paddingBottom: Sizes.fluid(48, 375),
			paddingLeft: Sizes.margin.thick,
			paddingRight: Sizes.margin.thick,
		},
		ayat: {
			paddingBottom: Sizes.fluid(32, 375),
		},
	},
})
