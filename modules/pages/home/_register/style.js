import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.white.primary,
		height: Sizes.app.width <= 414 ? undefined : Sizes.fluid(1000),
		justifyContent: 'center',
		paddingBottom: '10%',
		marginTop: '27%',
		paddingTop: Sizes.app.width <= 414 ? 74 : 180,
	},
	regis: {
		color: Colors.yellow.primary,
		position: 'absolute',
		top: Sizes.fluid(-130),
		alignSelf: 'center',
	},
	title: {
		position: 'absolute',
		top: Sizes.fluid(-60),
		alignSelf: 'center',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 650 * 1.5 : 650),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 135 * 1.5 : 135),
	},
	button: {
		paddingTop: Sizes.app.width <= 414 ? 32 : 150,
		alignSelf: 'center',
	},

	'@media (max-width: 414px)': {
		regis: {
			top: Sizes.fluid(-60, 414),
		},
	},
})
