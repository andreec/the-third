import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import PageContext from 'coeur/contexts/page';

import AxisBit from 'modules/bits/axis';
import AlvaroBit from 'modules/bits/alvaro';
import ButtonBit from 'modules/bits/button';
import BoxBit from 'modules/bits/box';

import Styles from './style';


export default ConnectHelper(
	class RegistrationPart extends StatefulModel {

		static contexts = [
			PageContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'onNavigateToRegister',
			])
		}

		onNavigateToRegister() {
			this.props.page.navigator.navigate('registration')
		}

		render() {
			return (
				<BoxBit unflex style={Styles.container}>
					<AxisBit
						type={AxisBit.TYPES.HERO}
						style={Styles.regis}
					>
						REGISTRATION
					</AxisBit>
					<AlvaroBit
						type={AlvaroBit.TYPES.HEADER_1}
						align="center"
						weight="bold"
					>
						Rp. 100.000
					</AlvaroBit>
					<ButtonBit
						title="BUY TICKETS"
						type={ButtonBit.TYPES.SHAPES.RECTANGLE}
						theme={ButtonBit.TYPES.THEMES.SECONDARY}
						size={ButtonBit.TYPES.SIZES.TINY}
						width={ButtonBit.TYPES.WIDTHS.FIT}
						style={Styles.button}
						onPress={this.onNavigateToRegister}
					/>
				</BoxBit>
			)
		}
	}
)
