import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		paddingTop: '2.6%',
		paddingLeft: Sizes.app.width <= 414
			? Sizes.margin.thick
			: Sizes.fluid(240),
		paddingRight: Sizes.app.width <= 414
			? Sizes.margin.thick
			: Sizes.fluid(240),
		paddingBottom: '9%',
	},

	background: {
		zIndex: 2,
	},

	speakers: {
		color: Colors.orange.primary,
		paddingTop: Sizes.fluid(84),
		paddingBottom: Sizes.fluid(24),
	},
	image: {
		width: Sizes.fluid(457),
		height: Sizes.fluid(526),
		alignSelf: 'center',
	},
	box: {
		width: Sizes.fluid(457),
	},

	title: {
		color: Colors.white.primary,
		paddingBottom: 22,
		paddingTop: 18,
	},
	content: {
		color: Colors.white.primary,
		paddingBottom: Sizes.app.width <= 414 ? 32 : undefined,
	},

	containerContent: {
		justifyContent: 'space-between',
	},

	logotype: {
		position: 'absolute',
		height: 481,
		width: 1515,
		top: -75,
		alignSelf: 'center',
	},

	element2: {
		position: 'absolute',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 1515 * 1.5 : 1515),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 481 * 1.5 : 481),
		alignSelf: 'center',
	},
	element1: {
		position: 'absolute',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 2222 * 1.5 : 2222),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 1920 * 1.5 : 1920),
		alignSelf: 'center',
	},
	elementCircle: {
		position: 'absolute',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 756 * 1.5 : 756),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 623 * 1.5 : 623),
		top: Sizes.fluid(-350),
		left: Sizes.fluid(-250),
	},
	elementSquare: {
		position: 'absolute',
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 756 * 1.5 : 756),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 623 * 1.5 : 623),
		top: Sizes.fluid(-140),
		right: Sizes.fluid(-70),
	},

	'@media (max-width: 414px)': {
		image: {
			width: Sizes.fluid(457 * 3),
			height: Sizes.fluid(526 * 3),
			alignSelf: 'center',
		},

		speakers: {
			paddingTop: 32,
			paddingBottom: 0,
		},

		box: {
			width: Sizes.fluid(457 * 3),
			alignSelf: 'center',
		},
	},
})
