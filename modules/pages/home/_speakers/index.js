import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Sizes from 'coeur/constants/size';

import AxisBit from 'modules/bits/axis';
import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import ImageBit from 'modules/bits/image';
import BaskervilleBit from 'modules/bits/baskerville';

import Styles from './style';

const Background = BoxImageBit.resolve('speakers/background-speakers.png')
const SpeakersLogotype = ImageBit.resolve('speakers/speakers-logotype.png')
const Speaker1 = ImageBit.resolve('speakers/speaker-1.png')
const Speaker2 = ImageBit.resolve('speakers/speaker-2.png')
const Speaker3 = ImageBit.resolve('speakers/speaker-3.png')
const Element1 = ImageBit.resolve('speakers/element-1.png')
const Element2 = ImageBit.resolve('speakers/element-2.png')
const ElementCircle = ImageBit.resolve('element-2.png')
const ElementSquare = ImageBit.resolve('element-1.png')


export default ConnectHelper(
	class SpeakersPart extends StatefulModel {

		constructor(p) {
			super(p, {
			}, [
				'contentRenderer',
			])

			this.content = [{
				title: 'Bias Widodo',
				content: 'Bias Widodo merupakan seorang pemimpin yang berfokus untuk membangkitkan dan membangun tim pelayanan di setiap lini dalam gereja anak muda. Bias merupakan pembicara yang memiliki pandangan unik dan kreatif terhadap firman Tuhan, sehingga mampu membawa anak-anak muda bertumbuh dan mencapai pada potensi maksimal dalam kehidupan mereka.',
				image: Speaker1,
				align: Sizes.app.width <= 414 ? 'center' : 'left',
			}, {
				title: 'Audrey Desiree S.',
				content: 'Audrey Desiree mendedikasikan hidupnya untuk melayani Tuhan dalam kegerakan anak-anak muda, khususnya di pelayanan Cellgroup. Lewat kepemimpinannya Cellgroup di God\'s DNA mengalami pertumbuhan yang signifikan. Audrey juga merupakan seorang pengajar Firman Tuhan yang relevan di kalangan anak-anak muda',
				align: 'center',
				image: Speaker2,
			}, {
				title: 'Andreas Klemens W.',
				content: 'Andreas Klemens adalah Teens dan Youth pastor dari God\'s DNA yang memiliki visi untuk membawa anak-anak muda menjadi serupa dan segambar dengan Kristus. Hatinya akan pelayanan anak-anak muda berfokus pada kebenaran dan hadirat Tuhan. Andreas juga mengembalai berbagai gereja anak-anak muda yang tersebar di beberapa wilayah seperti Bekasi, Tambun, Karawang, dan lainnya',
				align: Sizes.app.width <= 414 ? 'center' : 'right',
				image: Speaker3,
			}]
		}

		contentRenderer({title, content, align, image}, i) {
			return (
				<BoxBit unflex key={i} style={Styles.box}>
					<ImageBit
						source={ image }
						style={Styles.image}
					/>
					<BaskervilleBit italic
						type={BaskervilleBit.TYPES.HEADER_1}
						italic
						weight={Sizes.app.width <= 414 ? 'bold' : undefined}
						align={align}
						style={Styles.title}
					>
						{ title }
					</BaskervilleBit>
					<BaskervilleBit
						type={BaskervilleBit.TYPES.SUBHEADER_1}
						align={align}
						style={Styles.content}
					>
						{ content }
					</BaskervilleBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxImageBit
					source={ Background }
					style={Styles.background}
				>
					{ Sizes.app.width <= 414 ? undefined : (
						<BoxImageBit
							source={ Element1 }
							style={Styles.element1}
						/>
					) }
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ Element2 }
							style={Styles.logotype}
						/>
					) }
					<AxisBit
						type={AxisBit.TYPES.HERO}
						align="center"
						style={Styles.speakers}
					>
						SPEAKERS
					</AxisBit>
					<BoxBit style={Styles.container}>
						<BoxBit unflex row={Sizes.app.width <= 414 ? false : true} style={Styles.containerContent}>
							{ this.content.map(this.contentRenderer) }
						</BoxBit>
					</BoxBit>
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ ElementCircle }
							style={Styles.elementCircle}
						/>
					) }
					{ Sizes.app.width <= 414 ? undefined : (
						<ImageBit
							source={ ElementSquare }
							style={Styles.elementSquare}
						/>
					) }
				</BoxImageBit>
			)
		}
	}
)
