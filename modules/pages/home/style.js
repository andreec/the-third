import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.default.palette(4),
	},

	logoBlack: {
		height: Sizes.fluid(92),
		width: Sizes.fluid(92),
		marginTop: Sizes.fluid(34),
		left: Sizes.fluid(48),
		alignSelf: Sizes.app.width <= 414 && 'center',
	},

	containerImage: {
		paddingTop: Sizes.fluid(64),
	},
	logoType: {
		width: Sizes.fluid(989),
		height: Sizes.fluid(25),
		alignSelf: 'center',
	},
	theThird: {
		width: Sizes.fluid(906),
		height: Sizes.fluid(362),
		marginTop: Sizes.fluid(112),
		alignSelf: 'center',
	},
	date: {
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 938 * 1.5 : 938),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 132 * 1.5 : 132),
		marginTop: Sizes.fluid(42),
		left: 10,
		alignSelf: 'center',
	},


	buyTicket: {
		marginTop: Sizes.app.width <= 414
			? 36
			: 70,
		alignSelf: 'center',
	},
	background: {
		paddingBottom: Sizes.fluid(122),
	},
	element: {
		width: Sizes.fluid(Sizes.app.width <= 1023 ? 2571 * 1.5 : 2571),
		height: Sizes.fluid(Sizes.app.width <= 1023 ? 1123 * 1.5 : 1123),
		position: 'absolute',
		alignSelf: 'center',
	},

	imageAbout: {
		height: 529,
		width: 684,
	},
	imageQuoteAbout: {
		height: 171,
		width: 622,
	},
	elementAbout: {
		width: 756,
		height: 623,
		position: 'absolute',
		right: 0,
	},

	session: {
		width: 788,
		height: 119,
		marginTop: 282,
		marginBottom: 161,
	},
	workshop: {
		width: 950,
		height: 121,
		marginTop: 121,
		marginBottom: 161,
		alignSelf: 'flex-end',
	},
	contentImage: {
		width: 455,
		height: 551,
	},
	content: {
		paddingBottom: 161,
	},
	item: {
		marginLeft: 59,
		justifyContent: 'center',
	},
	itemRight: {
		marginRight: 59,
		justifyContent: 'center',
	},
	desc: {
		paddingTop: 24,
	},

	godsdnaImage: {
		width: 1388,
		height: 1157,
		justifyContent: 'center',
	},
	descGodsdna: {
		marginTop: 240,
		marginLeft: 120,
	},

	elementRight: {
		position: 'absolute',
		right: 0,
		bottom: -110,
		width: 435,
		height: 549,
	},

	'@media (max-width: 414px)': {
		background: {
			paddingBottom: Sizes.fluid(64, 375),
		},
		logoBlack: {
			height: Sizes.fluid(60, 375),
			width: Sizes.fluid(60, 375),
			alignSelf: 'center',
			left: 'unset',
			marginTop: Sizes.fluid(64, 375),
		},
		logoType: {
			left: 0,
			width: Sizes.fluid(795, 1024),
			height: Sizes.fluid(20, 375),
			marginTop: Sizes.fluid(64, 375),
			alignSelf: 'center',
		},
		theThird: {
			marginTop: Sizes.fluid(16, 375),
			width: Sizes.app.width - (Sizes.margin.thick * 2),
			height: Sizes.fluid(156, 375),
		},
		date: {
			marginTop: Sizes.fluid(32, 375),
			left: Sizes.fluid(10, 375),
			alignSelf: 'center',
			width: Sizes.fluid(938, 1024),
			height: Sizes.fluid(184, 1024),
		},

		buyTicket: {
			marginTop: Sizes.fluid(40, 375),
			alignSelf: 'center',
			width: 'unset',
		},
	},
})
