import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';


export default ConnectHelper(
	class LoaderPage extends PageModel {

		static routeName = 'loader'

		render() {
			return super.render(
				<BoxBit centering>
					<LoaderBit />
				</BoxBit>
			)
		}
	}
)
