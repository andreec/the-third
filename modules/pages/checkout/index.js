import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import Colors from 'coeur/constants/color';

import BaskervilleBit from 'modules/bits/baskerville';
import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import InputFileBit from 'modules/bits/input.file';
import IconBit from 'modules/bits/icon';
import PageBit from 'modules/bits/page';
import TextBit from 'modules/bits/text';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import AmountLego from 'modules/legos/amount';

import Styles from './style';


export default ConnectHelper(
	class CheckoutSuccessPage extends PageModel {

		static routeName = 'checkout.success'

		constructor(p) {
			super(p, {
			}, [
				'onNavigateToHome',
			])
		}

		onNavigateToHome() {
			this.navigator.navigate('')
		}

		render() {
			return super.render(
				<PageBit>
					<BoxBit style={Styles.container}>
						<IconBit
							name="checkmark"
							size={84}
							color={Colors.green.primary}
						/>
						<BaskervilleBit
							type={BaskervilleBit.TYPES.PARAGRAPH_2}
							align="center"
							style={Styles.title}
						>
							Terima kasih karna terlah menyelesaikan pembayaran untuk tiket Gods DNA Conference 2019.{'\n'}Tuhan Yesus memberkati
						</BaskervilleBit>
						<ButtonBit
							title="Back to home"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							width={ButtonBit.TYPES.WIDTHS.FIT}
							onPress={this.onNavigateToHome}
							style={Styles.button}
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
