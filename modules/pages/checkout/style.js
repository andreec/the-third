import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		paddingLeft: Sizes.app.width <= 414
			? Sizes.margin.thick
			: Sizes.fluid(240),
		paddingRight: Sizes.app.width <= 414
			? Sizes.margin.thick
			: Sizes.fluid(240),
		justifyContent: 'center',
	},
	title: {
		paddingTop: 32,
	},

	button: {
		marginTop: 32,
		alignSelf: 'center',
	},
})
