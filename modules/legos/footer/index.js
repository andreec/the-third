import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import UtilitiesContext from 'coeur/contexts/utilities';

import Linking from 'coeur/libs/linking';

import AlvaroBit from 'modules/bits/alvaro';
import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

const Logo = BoxImageBit.resolve('gdnawhite.png')
const TheThird = ImageBit.resolve('the-third-white.png')

const Phone = ImageBit.resolve('footer/phone.png')
const Location = ImageBit.resolve('footer/location.png')


export default ConnectHelper(
	class FooterLego extends StatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'listRenderer',
				'onNavigateToInstagram',
				'onNavigateToLine',
				'onNavigateToYoutube',
				'onNavigateToMail',
				'onNavigateToMaps',
				'onNavigateToWhatsapp',
			])

			this._list = [{
				title: '@godsdna',
				size: '35%',
				width: Sizes.fluid(51),
				height: Sizes.fluid(51),
				icon: 'instagram',
				onPress: this.onNavigateToInstagram,
			}, {
				title: 'contact@godsdna.org',
				size: '65%',
				width: Sizes.fluid(171),
				height: Sizes.fluid(97),
				icon: 'mail',
				onPress: this.onNavigateToMail,
			}, {
				title: '@piy2859c',
				size: '35%',
				width: Sizes.fluid(52),
				height: Sizes.fluid(51),
				icon: 'line',
				onPress: this.onNavigateToLine,
			}, {
				title: '+6285921541666 ( Fellisa Meliani )',
				size: '65%',
				width: Sizes.fluid(63),
				height: Sizes.fluid(64),
				source: Phone,
				onPress: this.onNavigateToWhatsapp,
			}, {
				title: 'godsdna',
				size: '35%',
				width: Sizes.fluid(71),
				height: Sizes.fluid(51),
				icon: 'youtube',
				onPress: this.onNavigateToYoutube,
			}, {
				title: 'GBI GRAHA\nBETHANY\n\nJL. PADJAJARAN KAV.8,\nLIPO CIKARANG,BEKASI. INDONESIA',
				size: '65%',
				width: Sizes.fluid(53),
				height: Sizes.fluid(65),
				source: Location,
				onPress: this.onNavigateToMaps,
			}]

			this.firstRow = [{
				title: '@godsdna',
				icon: 'instagram',
				onPress: this.onNavigateToInstagram,
			}, {
				title: '@piy2859c',
				icon: 'line',
				onPress: this.onNavigateToLine,
			}, {
				title: 'godsdna',
				icon: 'youtube',
				onPress: this.onNavigateToYoutube,
			}]

			this.secondRow = [{
				title: 'contact@godsdna.org',
				icon: 'mail',
				onPress: this.onNavigateToMail,
			}, {
				title: '+6285921541666 ( Fellisa Meliani )',
				source: Phone,
				width: 24,
				height: 24,
				onPress: this.onNavigateToWhatsapp,
			}, {
				title: 'GBI GRAHA\nBETHANY\n\nJL. PADJAJARAN KAV.8,\nLIPO CIKARANG,BEKASI. INDONESIA',
				width: 24,
				height: 24,
				source: Location,
				onPress: this.onNavigateToMaps,
			}]
		}

		onNavigateToInstagram() {
			Linking.open('https://www.instagram.com/godsdna/')
		}
		onNavigateToLine() {
			Linking.open('http://line.me/ti/p/~@piy2859c')
		}
		onNavigateToYoutube() {
			Linking.open('https://www.youtube.com/user/GodsDNAful')
		}
		onNavigateToMail() {
			Linking.open('mailto:contact@godsdna.org')
		}
		onNavigateToMaps() {
			Linking.open('https://goo.gl/maps/djMKZhXqgCGkCXx76')
		}
		onNavigateToWhatsapp() {
			Linking.open('https://api.whatsapp.com/send?phone=6285921541666&text=Hi%20saya%20mau%20daftar%20Conference.')
		}

		listRenderer({title, source, icon, size, width, height, onPress}, i) {
			return (
				<BoxBit unflex key={i} style={[Styles.list, {width: size}]}>
					<TouchableBit row unflex onPress={onPress}>
						{ !!source ? (
							<BoxBit unflex>
								<ImageBit
									source={ source }
									style={{
										width: width,
										height: height,
									}}
								/>
							</BoxBit>
						) : (
							<BoxBit unflex>
								<IconBit
									name={ icon }
									size={Sizes.app.width <= 414 ? 24 : Sizes.fluid(60)}
									color={Colors.white.primary}
								/>
							</BoxBit>
						) }
						<AlvaroBit
							type={AlvaroBit.TYPES.SUBHEADER_2}
							style={Styles.white}
						>
							{ title }
						</AlvaroBit>
					</TouchableBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit unflex style={Styles.container}>
					<BoxBit row unflex style={Styles.header}>
						{ Sizes.app.width <= 414 ? undefined : (
							<BoxBit row>
								<ImageBit
									source={ Logo }
									style={Styles.logo}
								/>
								<ImageBit
									source={ TheThird }
									style={Styles.theThird}
								/>
							</BoxBit>
						) }
						<BoxBit>
							<AlvaroBit
								type={AlvaroBit.TYPES.HEADER_4}
								weight="bold"
								align="center"
								style={Styles.title}
							>
								CONTACT
							</AlvaroBit>
						</BoxBit>
						{ Sizes.app.width <= 414 ? undefined : (
							<BoxBit />
						) }
					</BoxBit>
					{ Sizes.app.width <= 414 ? (
						<BoxBit unflex style={Styles.footer}>
							<BoxBit unflex style={Styles.firstRow}>
								{ this.firstRow.map(this.listRenderer) }
							</BoxBit>
							{ this.secondRow.map(this.listRenderer) }
						</BoxBit>
					) : (
						<BoxBit row unflex style={Styles.footer}>
							{ this._list.map(this.listRenderer) }
						</BoxBit>
					) }
				</BoxBit>
			)
		}
	}
)
