import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		// height: 700,
		paddingBottom: '7%',
		backgroundColor: Colors.black.primary,
	},
	logo: {
		height: 84,
		width: 84,
	},
	theThird: {
		height: 84,
		marginLeft: 8,
	},

	containerTab: {
		justifyContent: 'space-between',
	},

	white: {
		color: Colors.white.primary,
		paddingLeft: Sizes.app.width <= 414 ? 12 : 32,
		alignSelf: 'center',
	},

	title: {
		color: Colors.white.primary,
	},

	header: {
		paddingTop: 32,
		paddingBottom: Sizes.app.width <= 414 ? 32 : 100,
		paddingLeft: 32,
		paddingRight: 32,
		alignItems: Sizes.app.width <= 414 ? undefined : 'flex-end',
	},

	footer: {
		flexWrap: 'wrap',
		paddingLeft: Sizes.app.width < 1920
			? Sizes.fluid(300)
			: Sizes.fluid(580),
		paddingRight: Sizes.app.width < 1920
			? Sizes.fluid(180)
			: Sizes.fluid(194),
	},
	list: {
		// width: '50%',
		paddingTop: Sizes.app.width <= 414 ? 16 : 24,
		paddingBottom: Sizes.app.width <= 414 ? 16 : 24,
		justifyItems: 'center',
	},


	'@media (max-width: 414px)': {
		container: {
			paddingBottom: 32,
		},
		firstRow: {
			paddingLeft: Sizes.margin.thick,
			paddingRight: Sizes.margin.thick,
			alignItems: 'center',
			paddingBottom: 16,
		},
		list: {
			paddingTop: Sizes.fluid(12, 375),
			paddingBottom: Sizes.fluid(12, 375),
		},
		footer: {
			paddingLeft: Sizes.margin.thick,
			paddingRight: Sizes.margin.thick,
		},
	},
})
