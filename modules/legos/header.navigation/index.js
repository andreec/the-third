import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Sizes from 'coeur/constants/size';

import PageContext from 'coeur/contexts/page';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import BaskervilleBit from 'modules/bits/baskerville';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

const TheThird = ImageBit.resolve('the-third.png')


export default ConnectHelper(
	class HeaderNavigationPage extends StatefulModel {

		static contexts = [
			PageContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'tabRenderer',
				'onNavigateToRegister',
				'onNavigateToAbout',
				'onNavigateToSchedule',
				'onNavigateToFooter',
			])

			this._tab = [{
				title: 'ABOUT',
				onPress: this.onNavigateToAbout,
			}, {
				title: 'SPEAKERS',
				onPress: this.onNavigateToSpeakers,
			}, {
				title: 'SCHEDULE',
				onPress: this.onNavigateToSchedule,
			}, {
				title: 'REGISTRATION',
				onPress: this.onNavigateToRegister,
			}, {
				title: 'CONTACT',
				onPress: this.onNavigateToFooter,
			}]
		}

		onNavigateToRegister() {
			this.props.page.navigator.navigate('registration')
		}

		onNavigateToAbout() {
			window.scrollTo({
				// eslint-disable-next-line no-nested-ternary
				top: Sizes.app.width === 1920
					? 1135 + 155
					// eslint-disable-next-line no-nested-ternary
					: Sizes.app.width === 1440
						? 886 + 116
						// eslint-disable-next-line no-nested-ternary
						: Sizes.app.width === 1280
							? 803 + 103
							// eslint-disable-next-line no-nested-ternary
							: Sizes.app.width <= 1024
								? 673 + 82
								: 546 + 62,
				behavior: 'smooth',
			})
		}

		onNavigateToSpeakers() {
			window.scrollTo({
				// eslint-disable-next-line no-nested-ternary
				top: Sizes.app.width === 1920
					? 1135 + 166 + 1404
					// eslint-disable-next-line no-nested-ternary
					: Sizes.app.width === 1440
						? 886 + 116 + 1040
						// eslint-disable-next-line no-nested-ternary
						: Sizes.app.width === 1280
							? 803 + 103 + 972
							// eslint-disable-next-line no-nested-ternary
							: Sizes.app.width <= 1024
								? 673 + 82 + 770
								: 546 + 62 + 583,
				behavior: 'smooth',
			})
		}

		onNavigateToSchedule() {
			window.scrollTo({
				// eslint-disable-next-line no-nested-ternary
				top: Sizes.app.width === 1920
					? 1135 + 155 + 1404 + 1659
					// eslint-disable-next-line no-nested-ternary
					: Sizes.app.width === 1440
						? 886 + 116 + 1040 + 1250
						// eslint-disable-next-line no-nested-ternary
						: Sizes.app.width === 1280
							? 803 + 103 + 972 + 1113
							// eslint-disable-next-line no-nested-ternary
							: Sizes.app.width <= 1024
								? 673 + 82 + 770 + 925
								: 546 + 62 + 583 + 711,
				behavior: 'smooth',
			})
		}

		onNavigateToFooter() {
			window.scrollTo({
				top: 9000,
				behavior: 'smooth',
			})
		}

		tabRenderer({title, onPress}, i) {
			return (
				<TouchableBit unflex key={i} centering onPress={onPress}>
					<BaskervilleBit
						type={BaskervilleBit.TYPES.HEADER_3}
						style={Styles.title}
					>
						{ title }
					</BaskervilleBit>
				</TouchableBit>
			)
		}

		render() {
			return (
				<BoxBit row unflex style={Styles.container}>
					<BoxBit style={Styles.flex}>
						<ImageBit
							source={ TheThird }
							style={Styles.theThird}
						/>
					</BoxBit>
					<BoxBit row style={Styles.containerTab}>
						{ this._tab.map(this.tabRenderer) }
					</BoxBit>
					<BoxBit unflex style={Styles.flex} />
				</BoxBit>
			)
		}
	}
)
