import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

export default StyleSheet.create({
	container: {
		height: Sizes.fluid(155),
		paddingLeft: 32,
		paddingRight: 32,
		alignItems: 'center',
		backgroundColor: Colors.orange.palette(1, .85),
	},
	logo: {
		height: 84,
		width: 84,
	},
	theThird: {
		height: Sizes.fluid(76),
		marginLeft: 22,
		top: -4,
		alignSelf: 'flex-start',
	},

	containerTab: {
		justifyContent: 'space-around',
		flexGrow: 5,
		paddingLeft: 10,
	},
	flex: {
		flexGrow: 1,
	},
})
