import StyleSheet from 'coeur/libs/style.sheet';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';


export default StyleSheet.create({
	container: {
		paddingTop: Sizes.fluid(100),
	},
	white: {
		color: Colors.white.primary,
	},
	time: {
		paddingBottom: 8,
	},

	contentSchedule: {
		height: Sizes.fluid(150),
		alignSelf: 'flex-end',
		paddingBottom: 20,
	},

	padder: {
		paddingBottom: Sizes.fluid(60),
	},

	padderContent: {
		paddingLeft: Sizes.fluid(240),
	},

	'@media (max-width: 414px)': {
		container: {
			paddingTop: Sizes.fluid(64, 375),
		},
		contentSchedule: {
			height: 'unset',
			paddingBottom: Sizes.fluid(18, 375),
		},

		padderContent: {
			paddingTop: Sizes.fluid(24, 375),
			paddingLeft: Sizes.fluid(42, 375),
		},
	},
})
