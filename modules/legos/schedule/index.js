import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Sizes from 'coeur/constants/size'

import AxisBit from 'modules/bits/axis';
import BaskervilleBit from 'modules/bits/baskerville';
import BoxBit from 'modules/bits/box';

import Styles from './style';


export default ConnectHelper(
	class ScheduleLego extends StatefulModel {

		constructor(p) {
			super(p, {
			}, [
				'sessionRenderer',
			])

			this.day1 = [{
				title: 'DAY 1\n22 NOV 2019',
				time: '15.00',
				content: 'Registrasi',
			}, {
				title: '',
				time: '17.00',
				content: 'Sesi 1',
			}]

			this.day2 = [{
				title: 'DAY 2\n23 NOV 2019',
				time: '07.00',
				content: 'Registrasi',
			}, {
				title: '',
				time: '08.00',
				content: 'Sesi 2',
			}, {
				title: '',
				time: '10.25',
				content: 'Sesi 3',
			}, {
				title: '',
				time: '13.00',
				content: 'Workshop',
			}, {
				title: '',
				time: '14.00',
				content: 'Sesi 4',
			}]
		}

		sessionRenderer({title, time, content}, i) {
			return (
				<BoxBit key={i} row style={Styles.padder}>
					<BoxBit row>
						<BoxBit />
						<AxisBit
							type={AxisBit.TYPES.HEADER_2}
							style={Styles.white}
						>
							{ title }
						</AxisBit>
					</BoxBit>
					<BoxBit row style={Styles.padderContent}>
						<BoxBit style={Styles.contentSchedule}>
							<BoxBit />
							<BaskervilleBit
								weight="bold"
								type={BaskervilleBit.TYPES.HEADER_4}
								style={[Styles.white, Styles.time]}
							>
								{ time }
							</BaskervilleBit>
							<BaskervilleBit italic
								type={BaskervilleBit.TYPES.HEADER_4}
								weight="normal"
								style={Styles.white}
							>
								{ content }
							</BaskervilleBit>
						</BoxBit>
						{ Sizes.app.width <= 414
							? undefined
							: (
								<BoxBit />
							) }
					</BoxBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit style={Styles.container}>
					<BoxBit >
						{ this.day1.map(this.sessionRenderer) }
					</BoxBit>
					{ this.day2.map(this.sessionRenderer) }
				</BoxBit>
			)
		}
	}
)
