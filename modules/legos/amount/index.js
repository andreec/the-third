import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Sizes from 'coeur/constants/size'

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';
import TextInputBit from 'modules/bits/text.input';

import Styles from './style';


export default ConnectHelper(
	class AmountLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				value: PropTypes.number,
				onChange: PropTypes.func,
			}
		}

		static defaultProps = {
			value: 1,
		}

		constructor(p) {
			super(p, {
				amount: p.value,
			}, [
				'plus',
				'less',
			])
		}

		plus() {
			this.setState({
				amount: this.state.amount + 1,
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.state.amount)
			})
		}

		less() {
			this.setState({
				amount: Math.max(this.state.amount - 1, 0),
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.state.amount)
			})
		}

		onChange = (e, val) => {
			this.setState({
				amount: val,
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.state.amount || 0)
			})
		}

		render() {
			return (
				<BoxBit unflex row style={Styles.container}>
					<TextInputBit
						value={ this.state.amount }
						onChange={ this.onChange }
						style={ Styles.textInput }
						inputStyle={ Styles.input }
					/>
					<TouchableBit unflex style={Styles.icon} onPress={this.state.amount <= 1 ? undefined : this.less}>
						<IconBit
							name="substract"
							size={ Sizes.app.width <= 1024 ? 18 : 24 }
						/>
					</TouchableBit>
					<TouchableBit unflex onPress={this.plus} style={Styles.icon}>
						<IconBit
							name="expand"
							size={ Sizes.app.width <= 1024 ? 18 : 24 }
						/>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
