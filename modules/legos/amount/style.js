import StyleSheet from 'coeur/libs/style.sheet';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';


export default StyleSheet.create({
	container: {
		alignItems: 'center',
		paddingTop: 12,
		paddingBottom: 12,
	},
	icon: {
		marginLeft: 3,
		marginRight: 3,
		height: Sizes.app.width <= 414 ? Sizes.fluid(44, 414) : Sizes.fluid(50),
		width: Sizes.app.width <= 414 ? Sizes.fluid(44, 414) : Sizes.fluid(50),
		backgroundColor: Colors.grey.palette(2),
		borderRadius: 8,
		justifyContent: 'center',
	},
	textInput: {
		width: Sizes.app.width <= 414 ? Sizes.fluid(44, 414) : Sizes.fluid(60),
		height: Sizes.app.width <= 414 ? Sizes.fluid(44, 414) : Sizes.fluid(50),
		borderBottomWidth: Sizes.app.width <= 1024 ? 1 : 2,
		borderBottomColor: Colors.black.palette(2),
		marginRight: Sizes.fluid(18),
	},
	input: {
		textAlign: 'center',
	},
})
