import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	button: {
		paddingTop: 12,
		paddingBottom: 13,
		borderWidth: 1,
		borderColor: Colors.default.palette(5),
		borderRadius: 2,
		marginRight: 12,
	},
	text: {
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},
	buttonOk: {
		backgroundColor: Colors.primary,
	},
	buttonCancel: {
		backgroundColor: Colors.default.palette(2),
	},
	buttonInfo: {
		backgroundColor: Colors.default.palette(2),
	},
	ok: {
		color: Colors.default.palette(2),
	},
	cancel: {
		color: Colors.primary,
	},
	info: {
		color: Colors.primary,
	},
})
