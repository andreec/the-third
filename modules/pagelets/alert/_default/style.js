import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.default.palette(2),
		width: 312,
		borderRadius: 8,
	},

	content: {
		padding: Sizes.margin.thick,
	},

	icon: {
		marginBottom: 16,
	},

	title: {
		marginBottom: 8,
	},

	message: {
		color: Colors.default.palette(3, .8),
	},

	description: {
		color: Colors.default.palette(5),
		marginTop: 8,
	},

	radio: {
		marginTop: 8,
	},

	action: {
		flexDirection: 'row-reverse',
		paddingLeft: Sizes.margin.default,
		paddingRight: 4,
		paddingBottom: Sizes.margin.default,
	},
})
