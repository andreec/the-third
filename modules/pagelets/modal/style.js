import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	modal: {
		height: Sizes.app.height,
	},
	header: {
		backgroundColor: Colors.transparent,
	},
	container: {
		width: Sizes.app.width,
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
		backgroundColor: Colors.default.palette(2),
    	transform: Defaults.PLATFORM === 'web' ? 'translate3d(0, 0, 0)' : undefined,
	},
	content: {
		paddingTop: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 32,
	},
})
