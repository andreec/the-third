import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import HeaderBit from 'modules/bits/header';
import ScrollViewBit from 'modules/bits/scroll.view';

import Styles from './style';

export default ConnectHelper(
	class ModalPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onClose: PropTypes.func,
				children: PropTypes.node.isRequired,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {}, [
				'onClose',
			]);
		}

		header = {
			rightActions: [{
				type: HeaderBit.TYPES.CLOSE,
				onPress: this.props.onClose || this.onClose,
				data: {
					color: Colors.default.palette(2),
				},
			}],
			background: Colors.transparent,
		}

		onClose() {
			this.props.utilities.alert.hide()
		}

		render() {
			return (
				<BoxBit onLayout={this.props.onLayout} style={Styles.modal}>
					<HeaderBit {...this.header} />
					<ScrollViewBit
						style={Styles.container}
						contentContainerStyle={[Styles.content, this.props.style]}
						children={this.props.children}
					/>
				</BoxBit>
			)
		}
	}
)
