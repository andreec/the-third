import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';

import Styles from './style';


export default ConnectHelper(
	class EmptyPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				header: PropTypes.node,
				source: PropTypes.image,
				title: PropTypes.string,
				description: PropTypes.string,
				children: PropTypes.node,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: false,
		}

		render() {
			return(
				<BoxBit unflex={this.props.unflex} type={BoxBit.TYPES.THICK} style={[Styles.container, this.props.style]}>
					<BoxBit />
					{ this.props.header }
					{ this.props.source && (
						<BoxBit unflex centering style={Styles.content}>
							<ImageBit source={ this.props.source } broken={ !this.props.source } style={Styles.image}/>
						</BoxBit>
					) }
					<BoxBit style={Styles.padder} />
					<BoxBit unflex centering>
						<GeomanistBit type={GeomanistBit.TYPES.HEADER_4} style={Styles.title}>{ this.props.title }</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} style={Styles.desc}>{ this.props.description }</GeomanistBit>
						{ this.props.children }
					</BoxBit>
					<BoxBit />
				</BoxBit>
			)
		}
	}
)
