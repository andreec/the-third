import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'
// import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	container: {
		// width: Sizes.app.width,
		// height: Styles.size.fluidHeight,
		backgroundColor: Colors.default.palette(2),
	},
	content: {
		marginBottom: 24,
	},
	image: {
		width: 240,
		height: 240,
		borderRadius: 120,
		overflow: 'hidden',
	},
	title: {
		textAlign: 'center',
		alignSelf: 'stretch',
	},
	desc: {
		textAlign: 'center',
		paddingTop: 16,
		paddingBottom: 24,
	},
	padder: {
		flexGrow: .2,
	},
})
