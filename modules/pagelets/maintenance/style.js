import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	image: {
		width: 240,
		height: 240,
		borderRadius: 120,
		overflow: 'hidden',
	},
	desc: {
		textAlign: 'center',
		paddingTop: 16,
	},
})
