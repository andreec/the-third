import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import TrackingHandler from 'app/handlers/tracking';

// import PageContext from 'coeur/contexts/page';

import EmptyPagelet from '../empty';


export default ConnectHelper(
	class MaintenancePagelet extends StatefulModel {

		render() {
			return(
				<EmptyPagelet
					source="//app/404.jpg"
					title="Down for scheduled maintenance"
					description="We sincerely apologize for the inconvenience. Our site is currently under maintenance and will return shortly. Please try again later."
				/>
			)
		}
	}
)
