import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	selected: {
		backgroundColor: Colors.default.palette(5),
	},

	circleContainer: {
		width: 32,
		height: 32,
		marginRight: 12,
		borderWidth: 1,
		borderColor: Colors.default.palette(4),
		borderRadius: 16,
	},

	circleContainerActive: {
		backgroundColor: Colors.default.palette(3),
	},

	colorInactive: {
		color: Colors.default.palette(3, .7),
	},
	colorDark: {
		color: Colors.default.palette(3, .9),
	},
	colorRed: {
		color: Colors.default.palette(1),
	},
	colorBlue: {
		color: Colors.default.palette(1),
	},
})
