import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ButtonPart as CoreButtonPart,
} from 'coeur/modules/pagelets/menu';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'
import ImageBit from 'modules/bits/image'
import LineBit from 'modules/bits/line'
import RadioBit from 'modules/bits/radio'
import SwitchBit from 'modules/bits/switch'
import TouchableBit from 'modules/bits/touchable'

import Styles from './style'


export default ConnectHelper(
	class ButtonPart extends CoreButtonPart({
		Styles,
		BoxBit,
		GeomanistBit,
		IconBit,
		ImageBit,
		LineBit,
		RadioBit,
		SwitchBit,
		TextBit: GeomanistBit,
		TouchableBit,
	}) {
		iconTextRenderer(title, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PRIMARY_3} style={style}>{ title }</GeomanistBit>
			)
		}

		titleTextRenderer(title, style) {
			return (
				<GeomanistBit type={this.props.onTitlePress ? GeomanistBit.TYPES.SECONDARY_3 : GeomanistBit.TYPES.PARAGRAPH_3} style={style}>
					{ title }
				</GeomanistBit>
			)
		}

		typeTextRenderer(title, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={style}>{ title }</GeomanistBit>
			)
		}
	}
)
