import ConnectHelper from 'coeur/helpers/connect';
import CoreMenuPagelet from 'coeur/modules/pagelets/menu'

import BoxBit from 'modules/bits/box';
import TouchableBit from 'modules/bits/touchable';

import ListPart from './_list';

// import Styles from './style'


export {
	ListPart,
}

export default ConnectHelper(
	class MenuPagelet extends CoreMenuPagelet({
		// Styles,
		BoxBit,
		TouchableBit,
		ListPart,
	}) {
		constructor(p) {
			super(p)

			this.allowedGesture.DISMISS_BOTTOM = 0
		}
	}
)
