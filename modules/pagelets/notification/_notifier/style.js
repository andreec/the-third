import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: 17 + Sizes.safe.top,
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingBottom: 17,
		width: Sizes.app.width,
		alignItems: 'center',
	},
	background_DEFAULT: {
		backgroundColor: Colors.default.palette(1),
	},
	background_SUCCESS: {
		backgroundColor: Colors.default.palette(1),
	},

	text: {
		color: Colors.default.palette(2, .9),
	},

	close: {
		marginLeft: 8,
	},
})
