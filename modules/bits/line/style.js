import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';

export default StyleSheet.create({
	container: {
		borderWidth: 0,
		borderBottomWidth: 1,
		borderBottomColor: Colors.default.palette(5),
	},
})
