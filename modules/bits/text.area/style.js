import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Fonts from 'coeur/constants/font';

export default StyleSheet.create({
	container: {
		borderBottomWidth: 1,
		borderBottomColor: Colors.default.palette(5),
		transition: '.3s color, .3s border, .3s opacity',
	},
	focused: {
		// We need to override this because occurence order styling on web
		borderBottomColor: Colors.primary,
	},
	input: {
		height: 75,
		maxHeight: 106,
		resize: 'none',
		border: 0,
		paddingTop: 14,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: 13,
		fontFamily: Fonts.geo,
		fontSize: 18,
		fontWeight: '500',
		lineHeight: 24,
		letterSpacing: .4,
		color: Colors.default.palette(3, .7),
	},
})
