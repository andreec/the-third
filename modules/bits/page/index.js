import ConnectHelper from 'coeur/helpers/connect';
import CorePageBit from 'coeur/modules/bits/page';

import BoxBit from '../box';
import ScrollViewBit from '../scroll.view';

import Styles from './style'


export default ConnectHelper(
	class PageBit extends CorePageBit({
		// Styles,
		BoxBit,
		ScrollViewBit,
		Styles,
	}) {
		// static defaultProps = {
		// 	unflex: false,
		// 	scroll: true,
		// 	headerHeight: 48,
		// 	footerHeight: 50,
		// }
	}
)
