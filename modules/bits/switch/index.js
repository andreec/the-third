import ConnectHelper from 'coeur/helpers/connect';
import CoreSwitchBit from 'coeur/modules/bits/switch';

import Colors from 'coeur/constants/color';

import BoxBit from '../box'
import TouchableBit from '../touchable';

import Styles from './style'

export default ConnectHelper(
	class SwitchBit extends CoreSwitchBit({
		Styles,
		BoxBit,
		TouchableBit,
		tintColor: Colors.default.palette(1),
	}) {}
)
