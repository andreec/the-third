import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';

export default StyleSheet.create({
	container: {
		height: 32,
		width: 32,
		borderWidth: 2,
		borderColor: Colors.default.palette(5),

		transition: 'all .3s ease-in-out',
	},

	background: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,

		opacity: 1,

		borderRadius: 24,
		backgroundColor: Colors.default.palette(4),

		transition: 'all .3s ease-in-out',
	},

	knob: {
		position: 'absolute',
		left: -1,
		top: -1,

		width: 30,
		height: 30,

		borderRadius: 15,
		borderWidth: 1,
		borderColor: Colors.default.palette(5),

		backgroundColor: Colors.default.palette(2),

		transition: 'all .3s ease-in-out',
	},

	knobActive: {
		left: 19,
	},

	backgroundActive: {
		transform: 'scale(.2)',
	},
})
