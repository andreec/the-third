import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from '../../../constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderWidth: 0,
		borderColor: Colors.default.palette(5),
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
	},
})
