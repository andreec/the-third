import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

// import Colors from 'coeur/constants/color'
import Fonts from 'coeur/constants/font'

import TextBit from '../text'


export default ConnectHelper(
	class GeomanistBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.spec,
		weights: [
			'semibold',
			'bold',
			'extrabold',
		],
		TYPES: {
			HEADER_1: {
				fontSize: 44,
				lineHeight: 48,
				fontWeight: '700',
				letterSpacing: -1.6,
			},

			HEADER_2: {
				fontSize: 36,
				lineHeight: 36,
				fontWeight: '800',
				letterSpacing: -1.2,
			},

			HEADER_3: {
				fontSize: 30,
				lineHeight: 32,
				fontWeight: '800',
				letterSpacing: -1,
			},

			HEADER_4: {
				fontSize: 28,
				lineHeight: 32,
				fontWeight: '700',
				letterSpacing: -.8,
			},

			HEADER_5: {
				fontSize: 26,
				lineHeight: 32,
				fontWeight: '700',
				letterSpacing: -.8,

			},

			SUBHEADER_1: {
				fontSize: 22,
				lineHeight: 26,
				fontWeight: '600',
				letterSpacing: -.6,
			},

			SUBHEADER_2: {
				fontSize: 18,
				lineHeight: 22,
				fontWeight: '600',
				letterSpacing: -.4,
			},
		},
	}) {}
)
