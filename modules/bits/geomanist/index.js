import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

import Sizes from 'coeur/constants/size'
import Fonts from 'coeur/constants/font'

import TextBit from '../text'


export default ConnectHelper(
	class GeomanistBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.geo,
		weights: [
			'normal',
			'medium',
			'semibold',
			'bold',
		],
		TYPES: {
			// SAME AS P1
			DEFAULT: {
				fontSize: Sizes.app.width <= 414
					? 26
					: Sizes.fluid(66),
				lineHeight: Sizes.app.width <= 414
					? 26
					: Sizes.fluid(68),
				fontWeight: '800',
				letterSpacing: 11,
			},
			HEADER_1: {
				fontSize: Sizes.fluid(100),
				lineHeight: 102,
				fontWeight: '800',
				letterSpacing: 11,
			},
			HEADER_2: {
				fontSize: 36,
				lineHeight: 38,
				fontWeight: '500',
				letterSpacing: 11,
			},
			HEADER_3: {
				fontSize: Sizes.app.width <= 414
					? 32
					: Sizes.fluid(40),
				lineHeight: Sizes.app.width <= 414
					? 34
					: Sizes.fluid(42),
				fontWeight: '600',
				letterSpacing: 20,
			},

			PARAGRAPH_1: {
				fontSize: Sizes.app.width <= 414
					? 14
					: 20,
				lineHeight: Sizes.app.width <= 414
					? 18
					: 22,
				fontWeight: '600',
				letterSpacing: 10,
			},

			SUBHEADER_1: {
				fontSize: Sizes.app.width <= 414
					? 16
					: 22,
				lineHeight: 30,
				fontWeight: '600',
				letterSpacing: 10,
			},
		},
	}) {}
)
