import ConnectHelper from 'coeur/helpers/connect';
import CoreSpriteBit from 'coeur/modules/bits/sprite';

import ImageBit from '../image';


export default ConnectHelper(
	class SpriteBit extends CoreSpriteBit({
		ImageBit,
		Sprites : {
			'accessory' : ImageBit.resolve('sprites/lineup/accessory.png'),
			'clothing' : ImageBit.resolve('sprites/lineup/clothing.png'),
			'footwear' : ImageBit.resolve('sprites/lineup/footwear.png'),
			'bag': ImageBit.resolve('sprites/lineup/bag.png'),

			'bulb': ImageBit.resolve('sprites/bulb.png'),
			'referral': ImageBit.resolve('sprites/referral.png'),
			'checkmark': ImageBit.resolve('sprites/checkmarkSelected.png'),
		},
	}) {}
)
