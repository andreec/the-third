import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'
// import Fonts from 'coeur/constants/font'

export default StyleSheet.create({
	container: {
		height: 100,
		width: 500,
		backgroundColor: Colors.white.primary,
		borderWidth: 1,
		borderColor: Colors.black.primary,
	},
	border: {
		borderWidth: 1,
		borderColor: Colors.default.palette(5),
	},
})
