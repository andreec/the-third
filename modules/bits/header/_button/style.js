import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	textActive: {
		color: Colors.default.palette(3),
	},
	textInactive: {
		color: Colors.default.palette(5),
	},

	back: {
		backgroundColor: Colors.default.palette(2),
		borderRadius: 16,
		padding: 4,
	},
})
