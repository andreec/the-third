import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ButtonPart as CoreButtonPart,
} from 'coeur/modules/bits/header';

import Colors from '../../../../utils/constants/color';

import BoxBit from '../../box';
import GeomanistBit from '../../geomanist'
import IconBit from '../../icon'
import TouchableBit from '../../touchable'

import Styles from './style'

export default ConnectHelper(
	class ButtonPart extends CoreButtonPart({
		IconBit,
		TouchableBit,
	}) {
		static TYPES = {
			BACK: 'BACK_BUTTON',
			BLANK: 'BLANK',
			CLOSE: 'CLOSE_BUTTON',
			TIP: 'TIP',
			NOTIFICATION: 'NOTIFICATION',
			NEXT: 'NEXT',

			LOGO: 'LOGO',
			LOGO_YUNA: 'LOGO_YUNA',
			MENU: 'MENU_BUTTON',
			RESET: 'RESET_BUTTON',
			TEXT: 'TEXT',
			BACK_CIRCLE: 'BACK_CIRCLE',
		}

		static defaultProps = {
			data: {
				color: Colors.default.palette(3, .8),
			},
		}

		getIconName(type) {
			switch(type) {
			case this.TYPES.MENU:
				return 'menu';
			case this.TYPES.LOGO:
				return 'yuna';
			case this.TYPES.LOGO_YUNA:
				return 'yuna-gram';
			case this.TYPES.RESET:
				return 'refresh';
			case this.TYPES.CLOSE:
				return 'close';
			case this.TYPES.TIP:
				return 'tip';
			case this.TYPES.NOTIFICATION:
				return 'notification';
			case this.TYPES.NEXT:
				return 'arrow-tailed-right';
			default:
				return super.getIconName(type)
				// return 'tail-left';
			}
		}

		getOnPress(type) {
			switch(type) {
			case this.TYPES.BLANK:
			default:
				return undefined
			case this.TYPES.BACK:
			case this.TYPES.BACK_CIRCLE:
			case this.TYPES.CLOSE:
				return this.onNavigateBack
			}
		}

		buttonRenderer() {
			switch(this.props.type) {

			case this.TYPES.TEXT:
				return (
					<GeomanistBit type={GeomanistBit.TYPES.PRIMARY_2} weight={'semibold'} style={ this.state.isLoading || !this.state.isActive ? Styles.textInactive : Styles.textActive }>
						{ this.state.data }
					</GeomanistBit>
				)
			case this.TYPES.LOGO:
			case this.TYPES.LOGO_YUNA:
				return (
					<IconBit
						name={ this.getIconName(this.props.type) }
						color={ this.props.data && this.props.data.color }
						size={ 32 }
					/>
				)
			case this.TYPES.BACK_CIRCLE:
				return (
					<BoxBit unflex style={Styles.back}>
						<IconBit
							name="arrow-tailed-left"
							color={ this.props.data && this.props.data.color }
						/>
					</BoxBit>
				)
			default:
				return super.buttonRenderer()
			}
		}

	}
)
