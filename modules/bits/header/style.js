import StyleSheet from 'coeur/libs/style.sheet'
// import Defaults from 'coeur/constants/default'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	header: {
		minHeight: 48 + Sizes.safe.top,
		paddingLeft: 24,
		paddingRight: 24,
		width: Sizes.app.width,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	title: {
		marginTop: 3,
		marginBottom: 3,
		color: Colors.default.palette(3),
	},
})
