import ConnectHelper from 'coeur/helpers/connect';
import CoreClipboardBit from 'coeur/modules/bits/clipboard';

import IconBit from '../icon'
import TextAreaBit from '../text.area';
import TouchableBit from '../touchable';

import Styles from './style'


export default ConnectHelper(
	class ClipboardBit extends CoreClipboardBit({
		Styles,
		IconBit,
		TextAreaBit,
		TouchableBit,
		// iconColor
	}) {}
)
