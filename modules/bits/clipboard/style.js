import StyleSheet from 'coeur/libs/style.sheet'
// import ColorModel from 'coeur/models/color';
// import Sizes from '../../../constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		borderWidth: 0,
	},
	input: {
		height: 46,
		paddingTop: 11,
		paddingBottom: 11,
		color: Colors.default.palette(3, .8),
		fontSize: 16,
		fontWeight: '600',
	},
})
