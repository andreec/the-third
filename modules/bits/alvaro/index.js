import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

import Sizes from 'coeur/constants/size'
import Fonts from 'coeur/constants/font'

import TextBit from '../text'


export default ConnectHelper(
	class AlvaroBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.alv,
		weights: [
			'normal',
			'medium',
			'semibold',
			'bold',
		],
		TYPES: {
			// SAME AS P1
			DEFAULT: {
				fontSize: 14,
				lineHeight: 18,
				fontWeight: '400',
				letterSpacing: .4,
			},
			HEADER_1: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(32, 375)
					: Sizes.fluid(144),
				lineHeight: 146,
				fontWeight: '400',
				letterSpacing: .4,
			},
			HEADER_2: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(32, 375)
					: Sizes.fluid(60),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(40, 375)
					: Sizes.fluid(64),
				fontWeight: '400',
				letterSpacing: 1,
			},
			HEADER_4: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(32, 375)
					: Sizes.fluid(40),
				// lineHeight: Sizes.app.width <= 414
				// 	? Sizes.fluid(40, 375)
				// 	: Sizes.fluid(64),
				fontWeight: '400',
				letterSpacing: Sizes.fluid(20),
			},
			HEADER_3: {
				fontSize: 100,
				lineHeight: 102,
				fontWeight: '500',
				letterSpacing: 0,
			},

			SUBHEADER_1: {
				fontSize: 30,
				lineHeight: 34,
				fontWeight: '500',
				letterSpacing: 0,
			},
			SUBHEADER_2: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(16, 375)
					: 19,
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(20, 375)
					: 24,
				fontWeight: '600',
				letterSpacing: Sizes.app.width <= 414
					? 4
					: 8,
			},
		},
	}) {}
)
