import ConnectHelper from 'coeur/helpers/connect';
import CorePageAutohide from 'coeur/modules/bits/page.autohide';

import BoxBit from '../box';
import PageBit from '../page';

// import Styles from './style';


export default ConnectHelper(
	class PageAutohide extends CorePageAutohide({
		// Styles,
		BoxBit,
		PageBit,
	}) {}
)
