import ConnectHelper from 'coeur/helpers/connect';
import CoreCheckboxBit from 'coeur/modules/bits/checkbox';

import IconBit from '../icon'
import TouchableBit from '../touchable';

import Styles from './style'

export default ConnectHelper(
	class CheckboxBit extends CoreCheckboxBit({
		Styles,
		IconBit,
		TouchableBit,
	}) {}
)
