import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	input: {
		width: 28,
		textAlign: 'center',
		fontSize: 24,
		fontWeight: '700',
		color: Colors.default.palette(3, .9),
	},
	box: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 8,
		paddingRight: 8,
		borderWidth: 1,
		borderColor: Colors.default.palette(5),
		borderRadius: 4,
		marginRight: 12,
		width: 45,
	},
	focus: {
		borderColor: Colors.default.palette(1),
	},
})
