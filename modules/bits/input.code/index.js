import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';


import BoxBit from 'modules/bits/box';
import TextInputBit from 'modules/bits/text.input';
import Styles from './style';

import {
	padEnd,
} from 'lodash';


export default ConnectHelper(
	class InputCodeBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				value: PropTypes.oneOfType([
					PropTypes.number,
					PropTypes.string,
				]),
				count: PropTypes.number,
				onChange: PropTypes.func,
				onSubmitEditing: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			count: 4,
			value: '',
		}

		constructor(p) {
			super(p, {
				values: padEnd(p.value, p.count).split('').map(e => e.replace(' ', '')),
				focusIndex: -1,
			}, [
				'inputRenderer',
			]);

			this.inputs = []
		}

		bindInput(index, input) {
			this.inputs[index] = input
		}

		onChange(index, e, value) {
			if(value) {
				const values = this.state.values.slice()
				values[index] = value

				this.setState({
					values,
				})

				this.props.onChange &&
				this.props.onChange(values.join(''))

				if (index === this.props.count - 1) {
					this.inputs[index] &&
					this.inputs[index].blur()

					this.setState({
						focusIndex: -1,
					})

					this.props.onSubmitEditing &&
					this.props.onSubmitEditing(values.join(''))
				} else {
					this.inputs[index + 1] &&
					this.inputs[index + 1].focus()
				}
			}
		}

		onBackspace(index) {
			const values = this.state.values.slice()

			if(values[index]) {
				values[index] = ''
			} else if (index > 0) {
				this.inputs[index - 1] &&
				this.inputs[index - 1].focus()
			}

			this.setState({
				values,
			})
		}

		onFocus(i) {
			this.inputs[i] &&
			this.inputs[i].focus()

			this.setState({
				focusIndex: i,
			})
		}

		inputRenderer(_, i) {
			return (
				<TextInputBit
					key={ i }
					inputRef={this.bindInput.bind(this, i)}
					value={ this.state.values[i] }
					type={TextInputBit.TYPES.TEL}
					autocomplete={'off'}
					placeholder="-"
					maxlength={1}
					onChange={ this.onChange.bind(this, i) }
					onFocus={ this.onFocus.bind(this, i) }
					onBackspace={ this.onBackspace.bind(this, i) }
					style={[Styles.box, this.state.focusIndex === i && Styles.focus]}
					inputStyle={Styles.input}
				/>
			)
		}

		render() {
			return (
				<BoxBit unflex row centering style={this.props.style}>
					{ Array(this.props.count).fill(undefined).map(this.inputRenderer) }
				</BoxBit>
			);
		}
	}
);
