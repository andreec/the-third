import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import LangHelper from 'utils/helpers/lang';
import StatefulModel from 'coeur/models/stateful';

import Animated from 'coeur/libs/animated';

import CommonHelper from 'coeur/helpers/common';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ShadowBit from 'modules/bits/shadow';

import Styles from './style';


export default ConnectHelper(
	class InputRangeBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				value: PropTypes.number,
				snap: PropTypes.bool,
				stops: PropTypes.array,
				start: PropTypes.number,
				step: PropTypes.number,
				onChange: PropTypes.func,
				style: PropTypes.style,
				lang: PropTypes.object,
			};
		}

		static defaultProps = {
			value: null,
			stops: ['', ''],
			snap: true,
			start: 0,
			step: 1,
		};

		constructor(p) {
			super(p, {
				layout: {
					width: 0,
					halfWidth: 0,
					startPosition: 0,
					stopPosition: 0,
				},
			}, [
				'onMoveShouldSetPanResponder',
				'onPanResponderMove',
				'onPanResponderRelease',
				'snapToClosestPosition',
				'itemRenderer',
				'onLayout',
			]);

			this._animationValue = new Animated.Value(0);
			this._pressAnimationValue = new Animated.Value(1);
			this._pressAnimation = false

			this._animationInterpolation = {
				transform: [{
					translateX: this._animationValue,
				}, {
					scale: this._pressAnimationValue.interpolate({
						inputRange: [0, 1],
						outputRange: [.9, 1],
					}),
				}],
			};

			this._activeInterpolation = {
				opacity: 0,
			};

			this._panning = false;
			this._startX = 0;
			this._stopLength = p.stops.length;
			this._bullWidth = 8;
			this._buffer = 20;
		}

		onLayout(event) {
			const ll = event.nativeEvent.layout.width - this._bullWidth;
			const sl = ll / (this._stopLength - 1);

			const value = this.props.value === null ? ll * (this._stopLength - 1) / this._stopLength : sl * (this.props.value - this.props.start) / this.props.step

			this._animationValue.setValue(value)

			const stops = this.props.stops.map((stop, i) => {
				return i * sl
			})

			this._activeInterpolation = {
				opacity: this._animationValue.interpolate({
					inputRange: stops.map(stop => {
						return [ stop - this._buffer, stop, stop + this._buffer ]
					}).reduce((sum, arr) => {
						return sum.concat(arr)
					}, []),
					outputRange: stops.map(() => {
						return [0, 1, 0]
					}).reduce((sum, arr) => {
						return sum.concat(arr)
					}, []),
				}),
			};

			this.setState({
				layout: {
					width: ll,
					stopLength: sl,
					startPosition: 0,
					stopPosition: ll,
				},
			});
		}

		onMoveShouldSetPanResponder() {
			if (!this._panning) {
				this._panning = true;
				this._startX = this._animationValue._value;

				this.toggleTouch(true)
			}

			return true;
		}

		onPanResponderMove(e, gestureState) {
			// TODO : if exceed the boundry of total length, _value of animation is NaN, said there's problem with the 'coeur'
			this._animationValue.setValue(
				CommonHelper.clamp(
					this._startX + gestureState.dx,
					this.state.layout.startPosition,
					this.state.layout.stopPosition
				)
			);
		}

		onPanResponderRelease(e, gestureState) {
			let index = (CommonHelper.clamp(
				this._startX + gestureState.dx,
				this.state.layout.startPosition,
				this.state.layout.stopPosition
			)) / this.state.layout.stopLength

			if(this.props.snap) {
				index = Math.round(index)
				this.snapToClosestPosition(index * this.state.layout.stopLength, () => {
					this.props.onChange &&
					this.props.onChange(index * this.props.step + this.props.start)
				});
			} else {
				this.props.onChange &&
				this.props.onChange(index * this.props.step + this.props.start)
			}

			this.toggleTouch(false)
			this._panning = false;
		}

		snapToClosestPosition(cP, onCompleteCallback) {
			Animated.spring(this._animationValue, {
				toValue: cP,
				damping: 100,
			}).start(onCompleteCallback);
		}

		toggleTouch(isTouching) {
			this._pressAnimation &&
			this._pressAnimation.stop()

			this._pressAnimation = Animated.spring(this._pressAnimationValue, {
				toValue: isTouching ? 0 : 1,
				damping: 100,
			}).start()

		}

		itemRenderer(title, i) {
			return (
				<BoxBit key={i} centering style={[Styles.stop, i === 0 ? Styles.leftStop : i === this._stopLength - 1 && Styles.rightStop]}>
					<BoxBit unflex style={Styles.bull} />
					<BoxBit style={Styles.meter} />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.text} >
						{ LangHelper.getLang(this.props.lang, title) }
					</GeomanistBit>
				</BoxBit>
			);
		}

		render() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<ShadowBit x={0} y={3} blur={3} spread={0} animated style={[Styles.knob, this._animationInterpolation]}>
						<ShadowBit x={0} y={2} blur={2} spread={0} style={Styles.shadow}>
							<ShadowBit x={0} y={3} blur={1} spread={0} style={Styles.shadow}>
								<BoxBit animated onMoveShouldSetPanResponder={this.onMoveShouldSetPanResponder} onPanResponderMove={this.onPanResponderMove} onPanResponderRelease={this.onPanResponderRelease} style={[Styles.activeKnob, this._activeInterpolation]} />
							</ShadowBit>
						</ShadowBit>
					</ShadowBit>
					<BoxBit unflex row onLayout={this.onLayout}>
						{this.props.stops.map(this.itemRenderer)}
					</BoxBit>
				</BoxBit>
			);
		}
	}
);
