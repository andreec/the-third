import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingTop: 12,
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 12,
	},
	stop: {

	},
	leftStop: {
		alignItems: 'flex-start',
	},
	rightStop: {
		alignItems: 'flex-end',
	},
	meter: {
		top: 3,
		left: 0,
		right: 0,
		borderTopWidth: 2,
		borderColor: Colors.default.palette(5),
		position: 'absolute',
	},
	bull: {
		borderWidth: 2,
		borderColor: Colors.default.palette(5),
		borderRadius: 10,
		backgroundColor: Colors.default.palette(5),
		width: 8,
		height: 8,
	},
	knob: {
		position: 'absolute',
		top: 0,
		left: 0,
		height: 32,
		width: 32,
		borderRadius: 16,
		backgroundColor: Colors.default.palette(2),
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.default.palette(5),
		zIndex: 1,
	},
	activeKnob: {
		height: 32 - (2 * StyleSheet.hairlineWidth),
		width: 32 - (2 * StyleSheet.hairlineWidth),
		borderRadius: 15,
		backgroundColor: Colors.primary,
		alignItems: 'center',
	},
	shadow: {
		borderRadius: 16,
	},
	text: {
		marginTop: 17,
		color: Colors.default.palette(3),
		lineHeight: 20,
	},
	primary: {
		marginTop: 16,
		color: Colors.primary,
		height: 20,
	},
})
