import ConnectHelper from 'coeur/helpers/connect';
import CoreLinkBit from 'coeur/modules/bits/link';

export default ConnectHelper(
	class LinkBit extends CoreLinkBit() {}
)
