import ConnectHelper from 'coeur/helpers/connect';
import CoreLoaderBit from 'coeur/modules/bits/loader';

import Colors from 'coeur/constants/color'


export default ConnectHelper(
	class LoaderBit extends CoreLoaderBit({
		loaderColor: Colors.default.palette(3),
	}) {
		static defaultProps = {
			simple: true,
			size: 24,
		}
	}
)
