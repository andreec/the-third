import React from 'react';
import LangHelper from 'utils/helpers/lang';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Defaults from 'coeur/constants/default';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ScrollSnapBit from 'modules/bits/scroll.snap';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

import {
	debounce,
} from 'lodash';


export default ConnectHelper(
	class InputSliderBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				value: PropTypes.any,
				options: PropTypes.array.isRequired,
				onChange: PropTypes.func,
				style: PropTypes.style,
				lang: PropTypes.object,
			}
		}

		constructor(p) {
			super(p, {
				selectedIndex: p.value ? p.options.indexOf(p.value) : -1,
			}, [
				'onUpdateIndex',
				'itemRenderer',
			]);

			this.onUpdateIndex = debounce(this.onUpdateIndex, 500)
		}

		onUpdateIndex(e, index) {
			this.setState({
				selectedIndex: index,
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.props.options[index])
			})
		}

    itemRenderer(title, i) {
			return Defaults.PLATFORM === 'web' ? (
				<TouchableBit key={i} centering style={[Styles.selection, i === this.state.selectedIndex && Styles.selected]} onPress={ this.onUpdateIndex.bind(this, null, i) }>
					<GeomanistBit
						type={GeomanistBit.TYPES.SECONDARY_2}
						weight={'medium'}
						style={[Styles.text, i === this.state.selectedIndex && Styles.selectedText || undefined]}
					>
						{ LangHelper.getLang(this.props.lang, title) }
					</GeomanistBit>
				</TouchableBit>
			) : (
				<BoxBit key={i} centering style={[Styles.selection, i === this.state.selectedIndex && Styles.selected]}>
					<GeomanistBit
						type={GeomanistBit.TYPES.SECONDARY_2}
						weight={'medium'}
						style={[Styles.text, i === this.state.selectedIndex && Styles.selectedText || undefined]}
					>
						{ LangHelper.getLang(this.props.lang, title) }
					</GeomanistBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit row unflex style={[Styles.container, this.props.style]}>
					<ScrollSnapBit centering
						snapToInterval={84}
						index={this.state.selectedIndex}
						onMomentumScrollEnd={this.onUpdateIndex}
					>
						{ this.props.options.map(this.itemRenderer) }
					</ScrollSnapBit>
				</BoxBit>
			);
		}
	}
);
