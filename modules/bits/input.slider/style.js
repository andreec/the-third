import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		marginTop: -4,
		marginBottom: -4,
	},
	selection: {
		// for top and bottom margin in case of flex wrapping
		margin: 4,
		height: 44,
		width: 76,
		justifyContent: 'center',
		paddingLeft: 15,
		paddingRight: 15,
		borderWidth: 1,
		borderColor: Colors.default.palette(5),
		borderRadius: 22,
	},
	selected: {
		borderWidth: 1,
		borderColor: Colors.default.palette(5),
		backgroundColor: Colors.primary,
	},
	text: {
		color: Colors.default.palette(3, .6),
	},
	selectedText: {
		color: Colors.default.palette(2),
	},
})
