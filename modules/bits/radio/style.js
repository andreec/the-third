import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	inactive: {
		borderWidth: 1,
		borderColor: Colors.default.palette(5),
	},
})
