import StyleSheet from 'coeur/libs/style.sheet';

import Colors from 'coeur/constants/color';
import Fonts from 'coeur/constants/font';
import Sizes from 'coeur/constants/size';


export default StyleSheet.create({
	container: {
		height: Sizes.app.width <= 414 ? Sizes.fluid(44, 414) : Sizes.fluid(72),
		borderBottomWidth: Sizes.app.width <= 1024 ? 1 : 2,
		borderBottomColor: Colors.black.palette(2),
		transition: '.3s color, .3s border, .3s opacity',
	},
	focused: {
		// We need to override this because occurence order styling on web
		borderColor: Colors.black.palette(2),
	},
	input: {
		border: 0,
		padding: 0,
		fontFamily: Fonts.lib,
		fontSize: Sizes.app.width <= 414 ? Sizes.fluid(16, 414) : Sizes.fluid(30),
		fontWeight: '500',
		lineHeight: Sizes.app.width <= 414 ? Sizes.fluid(16, 414) : Sizes.fluid(38),
		letterSpacing: .4,
		color: Colors.black.palette(2, .8),
		width: '100%',
	},
})
