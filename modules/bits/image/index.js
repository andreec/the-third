import ConnectHelper from 'coeur/helpers/connect';
import CoreImageBit from 'coeur/modules/bits/image';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import IconBit from '../icon';
import LoaderBit from '../loader';


export default ConnectHelper(
	class ImageBit extends CoreImageBit({
		BoxBit,
		IconBit,
		LoaderBit,
		imageResolver: function(path) {
			return import(
				// cannot set it by importing from package json, it is webpack limitation
				/* webpackMode: "eager" */
				`@thethird/assets/img/${path}`
			).catch(err => {
				console.log(err)
				return Promise.resolve(false)
			})
		},
		backgroundColor: Colors.default.palette(4),
		indicatorColor: Colors.default.palette(5),
	}) {}
)
