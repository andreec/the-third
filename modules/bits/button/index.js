import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ButtonGhost from './_ghost';
import ButtonProgress from './_progress';
import ButtonRectangle from './_rectangle';


const SHAPES = {
	GHOST: 'GHOST',
	PROGRESS: 'PROGRESS',
	RECTANGLE: 'RECTANGLE',
}


export default ConnectHelper(
	class ButtonBit extends StatefulModel {

		static TYPES = {
			...ButtonRectangle.TYPES,
			THEMES: {
				...ButtonGhost.TYPES.THEMES,
				...ButtonProgress.TYPES.THEMES,
				...ButtonRectangle.TYPES.THEMES,
			},
			SIZES: {
				...ButtonGhost.TYPES.SIZES,
				...ButtonProgress.TYPES.SIZES,
				...ButtonRectangle.TYPES.SIZES,
			},
			SHAPES,
		}

		static propTypes(PropTypes) {
			return {
				...ButtonRectangle.propTypes,
				theme: PropTypes.oneOf(this.TYPES.THEMES),
				size: PropTypes.oneOf(this.TYPES.SIZES),
				type: PropTypes.oneOf(this.TYPES.SHAPES),
			}
		}

		static defaultProps = {
			...ButtonRectangle.defaultProps,
			type: SHAPES.RECTANGLE,
			weight: 'semibold',
		}

		render() {
			const {
				type,
				...props
			} = this.props

			switch(type) {
			case this.TYPES.SHAPES.GHOST:
				return (
					<ButtonGhost { ...props } />
				)
			case this.TYPES.SHAPES.PROGRESS:
				return (
					<ButtonProgress { ...props } />
				)
			case this.TYPES.SHAPES.RECTANGLE:
			default:
				return (
					<ButtonRectangle { ...props } />
				)
			}
		}
	}
)
