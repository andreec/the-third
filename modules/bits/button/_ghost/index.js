import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonBit from 'coeur/modules/bits/button'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import GeomanistBit from '../../geomanist'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonGhost extends CoreButtonBit({
		BoxBit,
		LoaderBit,
		TextBit: GeomanistBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.primary, Colors.transparent],
				ACTIVE: [Colors.primary, Colors.transparent],
				DISABLED: [Colors.default.palette(5), Colors.transparent],
				LOADING: [Colors.primary, Colors.transparent, undefined, Colors.primary],
			},
		},
		SIZES: {
			NORMAL: Styles.content,
			COMPACT: Styles.compact,
			medium: Styles.medium,
		},
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			return (
				<GeomanistBit weight={this.props.weight} type={GeomanistBit.TYPES.SECONDARY_3} style={[textStyle, this.props.textStyle, { color: this.state.color }]}>{ this.props.title }</GeomanistBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}
	}
)
