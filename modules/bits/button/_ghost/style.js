import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	content: {
		height: 44,
		paddingLeft: 0,
		paddingRight: 0,
	},
	compact: {
		height: 32,
		paddingLeft: 0,
		paddingRight: 0,
	},
	medium: {
		height: 36,
		paddingLeft: 0,
		paddingRight: 0,
	},
})
