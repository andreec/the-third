import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonProgressBit from 'coeur/modules/bits/button.progress'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import BaskervilleBit from '../../baskerville'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonGhost extends CoreButtonProgressBit({
		BoxBit,
		LoaderBit,
		TextBit: BaskervilleBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.white.primary, Colors.black.primary],
				ACTIVE: [Colors.white.primary, Colors.black.palette(1, .7)],
				DISABLED: [Colors.white.palette(2), Colors.black.palette(1, .2)],
				LOADING: [Colors.white.palette(2), Colors.black.palette(1, .9), undefined, Colors.black.palette(1)],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
				iconSize: PropTypes.number,
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = BaskervilleBit.TYPES.REGIS_PARAGRAPH
				break;
			}

			return (
				<BaskervilleBit weight={this.props.weight} type={textType} style={[textStyle, this.props.textStyle, { color: this.state.color }, Styles.title]}>{ this.props.title }</BaskervilleBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={this.props.iconSize}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
