import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		borderRadius: 2,
		borderWidth: 1,
	},

	compact: {
		height: 52,
		paddingLeft: 16,
		paddingRight: 16,
	},

	mini: {
		height: 42,
		paddingLeft: 16,
		paddingRight: 16,
	},

	normal: {
		height: Sizes.app.width <= 414 ? 44 : Sizes.fluid(84),
		paddingLeft: Sizes.app.width <= 414 ? 16 : Sizes.fluid(64),
		paddingRight: Sizes.app.width <= 414 ? 16 : Sizes.fluid(64),
	},

	tiny: {
		height: 36,
		paddingLeft: 22,
		paddingRight: 22,
	},

	title: {
		letterSpacing: 1.2,
		lineHeight: 16,
	},
})
