import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonBit from 'coeur/modules/bits/button'

import Colors from 'coeur/constants/color'

import AlvaroBit from '../../alvaro'
import BoxBit from '../../box'
import IconBit from '../../icon'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonGhost extends CoreButtonBit({
		BoxBit,
		LoaderBit,
		TextBit: AlvaroBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.white.primary, Colors.transparent, Colors.white.palette(1)],
				ACTIVE: [Colors.white.palette(1, .8), Colors.transparent, Colors.white.palette(1, .8)],
				DISABLED: [Colors.default.palette(2), Colors.default.palette(5), Colors.default.palette(5)],
				LOADING: [Colors.default.palette(2), Colors.primary, Colors.default.palette(5), Colors.primary],
			},
			SECONDARY: {
				NORMAL: [Colors.black.primary, Colors.transparent, Colors.black.primary],
				ACTIVE: [Colors.black.palette(1, .8), Colors.transparent, Colors.black.palette(1, .8)],
				DISABLED: [Colors.default.palette(2), Colors.default.palette(5), Colors.default.palette(5)],
				LOADING: [Colors.default.palette(2), Colors.primary, Colors.default.palette(5), Colors.primary],
			},
		},
		SIZES: {
			COMPACT: Styles.compact,
			MINI: Styles.mini,
			NORMAL: Styles.normal,
			TINY: Styles.tiny,
			SMALL: Styles.small,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}


		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.COMPACT:
				textType = AlvaroBit.TYPES.PRIMARY_2
				break;
			case this.TYPES.SIZES.MINI:
				textType = AlvaroBit.TYPES.NOTE_1
				break;
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = AlvaroBit.TYPES.SUBHEADER_1
				break;
			case this.TYPES.SIZES.TINY:
				textType = AlvaroBit.TYPES.SUBHEADER_2
			}

			return (
				<AlvaroBit weight={this.props.weight} type={textType} style={[textStyle, this.props.textStyle, { color: this.state.color }]}>{ this.props.title }</AlvaroBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
