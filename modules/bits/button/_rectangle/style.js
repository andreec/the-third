import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderRadius: 40,
		borderWidth: Sizes.app.width <= 414
			? 2
			: 4,
	},

	compact: {
		height: 52,
		paddingLeft: 16,
		paddingRight: 16,
	},

	mini: {
		height: 42,
		paddingLeft: 16,
		paddingRight: 16,
	},

	small: {
		height: 34,
		paddingLeft: 16,
		paddingRight: 16,
	},

	normal: {
		height: 70,
		paddingLeft: 16,
		paddingRight: 16,
	},

	tiny: {
		// eslint-disable-next-line no-nested-ternary
		height: Sizes.app.width <= 414
			? Sizes.fluid(42, 375)
			: Sizes.app.width <= 1025
				? Sizes.fluid(72, 1025)
				: Sizes.fluid(72),
		paddingLeft: 28,
		paddingRight: 22,
	},
})
