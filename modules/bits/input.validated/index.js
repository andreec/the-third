import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import CoreInputValidatedBit from 'coeur/modules/bits/input.validated';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import BaskervilleBit from '../baskerville'
import GeomanistBit from '../geomanist';
import IconBit from '../icon';
import SelectionBit from '../selection';
import TextAreaBit from '../text.area';
import TextInputBit from '../text.input';
import TouchableBit from '../touchable';

import Styles from './style';


export default ConnectHelper(
	class InputValidatedBit extends CoreInputValidatedBit({
		Styles,
		BoxBit,
		IconBit,
		SelectionBit,
		TextBit: BaskervilleBit,
		TextAreaBit,
		TextInputBit,
		TouchableBit,
		checkmarkColor: Colors.default.palette(1),
		visibilityColor: Colors.default.palette(3, .8),
	}) {
		titleRenderer(title, style) {
			return (
				<BaskervilleBit type={BaskervilleBit.TYPES.SUBHEADER_2} weight="medium" style={style}>{ title }</BaskervilleBit>
			)
		}

		counterRenderer(counter, style) {
			return (
				<BaskervilleBit type={BaskervilleBit.TYPES.NOTE_1} style={style}>
					{ counter }
				</BaskervilleBit>
			)
		}

		descriptionRenderer(style) {
			return (
				<BaskervilleBit type={BaskervilleBit.TYPES.NOTE_1} weight="normal" style={style}>{ this.props.description }</BaskervilleBit>
			)
		}

		prefixTextRenderer(prefix, style) {
			return (
				<BaskervilleBit type={BaskervilleBit.TYPES.REGIS_INPUT} weight="normal" style={style}>{ prefix }</BaskervilleBit>
			)
		}
	}
)
