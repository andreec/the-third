import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	title: {
		color: Colors.default.palette(3, .6),
		lineHeight: 16,
	},

	prefix: {
		marginRight: 8,
		color: Colors.default.palette(3, .8),
	},

	note: {
		marginTop: 8,
		marginBottom: 8,
		marginLeft: 0,
		marginRight: 0,
		color: Colors.default.palette(3, .6),
	},

	titleDisabled: {
		color: Colors.default.palette(3, .2),
	},

	titleReadonly: {
	},

	counter: {
		color: Colors.default.palette(3, .2),
	},

	inputInvalid: {
		color: Colors.default.palette(1),
	},

	inputValid: {
		color: Colors.default.palette(1),
	},
})
