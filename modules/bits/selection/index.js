import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import CoreSelectionBit from 'coeur/modules/bits/selection';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import GeomanistBit from '../geomanist';
import IconBit from '../icon';
import TextBit from '../text';
import TextInputBit from '../text.input';
import TouchableBit from '../touchable';


export default ConnectHelper(
	class SelectionBit extends CoreSelectionBit({
		BoxBit,
		IconBit,
		TextBit,
		TextInputBit,
		TouchableBit,
		iconDisabledColor: Colors.default.palette(3),
	}) {
		textRenderer(value, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} style={style}>{ value }</GeomanistBit>
			)
		}
	}
)
