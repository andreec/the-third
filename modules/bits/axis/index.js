import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

import Sizes from 'coeur/constants/size'
import Fonts from 'coeur/constants/font'

import TextBit from '../text'


export default ConnectHelper(
	class AxisBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.axs,
		weights: [
			'normal',
			'medium',
			'semibold',
			'bold',
		],
		TYPES: {
			// SAME AS P1
			DEFAULT: {
				fontSize: 14,
				lineHeight: 18,
				fontWeight: '400',
				letterSpacing: .4,
			},
			HERO: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(100, 414)
					: Sizes.fluid(200),
				// lineHeight: Sizes.app.width <= 414
				// 	? 66
				// 	: Sizes.fluid(204),
				fontWeight: '500',
				letterSpacing: Sizes.app.width <= 414
					? -2
					: 0,
			},
			HEADER_1: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(52, 375)
					: Sizes.fluid(90),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(56, 375)
					: Sizes.fluid(94),
				fontWeight: '500',
				letterSpacing: 0,
			},
			HEADER_2: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(32, 375)
					: Sizes.fluid(80),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(34, 375)
					: Sizes.fluid(100),
				fontWeight: '500',
				letterSpacing: 1,
			},
			PARAGRAPH_1: {
				fontSize: Sizes.app.width <= 414
					? 32
					: 48,
				lineHeight: Sizes.app.width <= 414
					? 18
					: 50,
				fontWeight: '500',
				letterSpacing: 1,
			},
		},
	}) {}
)
