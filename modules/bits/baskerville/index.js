/* eslint-disable no-nested-ternary */
import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

// import Colors from 'coeur/constants/color'
import Fonts from 'coeur/constants/font'
import Sizes from 'coeur/constants/size'

import TextBit from '../text'


export default ConnectHelper(
	class BaskervilleBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.lib,
		weights: [
			'normal',
			'bold',
		],
		TYPES: {
			// SAME AS P1
			DEFAULT: {
				fontSize: 14,
				lineHeight: 18,
				fontWeight: '400',
				letterSpacing: .4,
			},
			HERO: {
				fontSize: 26,
				// lineHeight: 18,
				fontWeight: '700',
				letterSpacing: .4,
			},
			HEADER_1: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(24, 375)
					: Sizes.fluid(42),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(32, 375)
					: Sizes.fluid(65),
				fontWeight: '700',
				letterSpacing: 2,
			},
			HEADER_2: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(18, 375)
					: Sizes.fluid(38),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(26, 375)
					: Sizes.fluid(65),
				fontWeight: '700',
				letterSpacing: .4,
			},
			HEADER_3: {
				fontSize: Sizes.app.width === 1280
					? 20
					: Sizes.fluid(30),
				lineHeight: Sizes.fluid(32),
				fontWeight: '700',
				letterSpacing: .4,
			},

			HEADER_4: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(18, 375)
					: Sizes.fluid(42),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(20, 375)
					: Sizes.fluid(46),
				fontWeight: '400',
				letterSpacing: .4,
			},

			SUBHEADER_1: {
				fontSize: Sizes.app.width <= 414
					? 14
					: Sizes.fluid(23),
				lineHeight: Sizes.app.width <= 414
					? 22
					: Sizes.fluid(35),
				fontWeight: '400',
				letterSpacing: .4,
			},

			SUBHEADER_2: {
				fontSize: 28,
				lineHeight: 32,
				fontWeight: '400',
				letterSpacing: .4,
			},
			PARAGRAPH_1: {
				fontSize: 13,
				lineHeight: 18,
				fontWeight: '700',
				letterSpacing: .4,
			},
			PARAGRAPH_2: {
				fontSize: 20,
				lineHeight: 28,
				fontWeight: '700',
				letterSpacing: .4,
			},
			PARAGRAPH_3: {
				fontSize: 16,
				lineHeight: 18,
				fontWeight: '500',
				letterSpacing: .4,
			},

			REGIS_PARAGRAPH: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(16, 414)
					: Sizes.fluid(34),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(24, 414)
					: Sizes.fluid(49),
				fontWeight: '400',
				letterSpacing: .4,
			},

			REGIS_PARAGRAPH_2: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(16, 414)
					: Sizes.fluid(28),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(24, 414)
					: Sizes.fluid(32),
				fontWeight: '400',
				letterSpacing: .4,
			},

			REGIS_INPUT: {
				fontSize: Sizes.app.width <= 414
					? Sizes.fluid(16, 414)
					: Sizes.fluid(30),
				lineHeight: Sizes.app.width <= 414
					? Sizes.fluid(22, 414)
					: Sizes.fluid(38),
				fontWeight: '400',
				letterSpacing: .4,
			},
		},
	}) {}
)
