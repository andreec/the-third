import React from 'react'
import Root from 'coeur/kernel/root'

import Defaults from 'coeur/constants/default'

// import MeManager from 'app/managers/me';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';

import AlertPagelet from 'modules/pagelets/alert';
import MenuPagelet from 'modules/pagelets/menu';
import NotificationPagelet from 'modules/pagelets/notification';

import Router from './router'


export default class Weblique extends Root {

	initialize(Store) {
		if(window) {
			// window.screen
			// && window.screen.orientation
			// && window.screen.orientation.lock
			// && window.screen.orientation.lock('portrait').catch(err => {
			// 	if(Defaults.DEBUG) {
			// 		console.warn(err)
			// 	}
			// })

			// this.state.isPortrait = window.orientation !== -90 && window.orientation !== 90
		}

		return new Store([
			// MeManager,
		])
	}

	// initializeGraphQL(GraphQL) {
	// 	return GraphQL.init(process.env.CONNECTER_URL + process.env.GRAPHQL_PATH)
	// }
	initializeGraphQL(GraphQL) {
		return GraphQL.init(process.env.CONNECTER_URL + process.env.VIEWQL_PATH)
	}

	onAppStateChange() {
		super.onAppStateChange(document.visibilityState)
	}

	onConnectivityChange(connection) {
		super.onConnectivityChange(connection.type !== 'offline')
	}

	onOrientationChange() {
		super.onOrientationChange(window.orientation)
	}

	componentDidMount() {
		// MeManager.onLoginStateChange = this.onLoginStateChange
		document.addEventListener('visibilitychange', this.onAppStateChange);
		window.addEventListener('online', this.onConnectivityChange);
		window.addEventListener('offline', this.onConnectivityChange);
		window.addEventListener('orientationchange', this.onOrientationChange);
	}

	componentWillUnmount() {
		document.removeEventListener('visibilitychange', this.onAppStateChange);
		window.removeEventListener('online', this.onConnectivityChange);
		window.removeEventListener('offline', this.onConnectivityChange);
		window.removeEventListener('orientationchange', this.onOrientationChange);
	}

	registerUtilities() {
		super.registerUtilities({
			MENU_TYPES: MenuPagelet.TYPES,
			NOTIFICATION_TYPES: NotificationPagelet.TYPES,
		})
	}

	render() {
		return super.render({
			Navigator: function({ state }) {
				return state.isPortrait ? (
					<Router.HashRouter>
						<Router.Route />
					</Router.HashRouter>
				) : (
					<BoxBit centering>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} align="center">
							This app only works in portrait mode.
							{'\n'}
							Please rotate your phone.
						</GeomanistBit>
					</BoxBit>
				)
			},
			AlertPagelet,
			MenuPagelet,
			NotificationPagelet,
			BoxBit,
			LoaderBit,
		})
	}
}
