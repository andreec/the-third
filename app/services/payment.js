import ServiceModel from 'coeur/models/service';


class PaymentService extends ServiceModel {

	static displayName = 'payment'

	constructor() {
		super(process.env.API_URL)
	}

	order(data) {
		return this.post('order', {
			data,
		})
	}

}

export default new PaymentService()
