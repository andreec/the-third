import RecordModel from 'coeur/models/record'

export default function UserRecordFactory(additionalConfig = {}) {
	return class UserRecord extends RecordModel('user', {
		id				: RecordModel.TYPES.ID,
		firstName		: RecordModel.TYPES.STRING,
		lastName		: RecordModel.TYPES.STRING,

		profileMediaId	: RecordModel.TYPES.ID,
		coverMediaId	: RecordModel.TYPES.ID,

		// bio				: RecordModel.TYPES.STRING,
		// avatar			: RecordModel.TYPES.STRING,
		// cover			: RecordModel.TYPES.STRING,
		// outfitIds		: RecordModel.TYPES.ARRAY,

		...additionalConfig,
	}) {
		get fullName() {
			return `${this.firstName} ${this.lastName}`
		}
	}
}
