/*
|--------------------------------------------------------------------------
| START JS
|--------------------------------------------------------------------------
|
| Put what needs to be loaded first here
|
*/

/*
| Extending constants
*/
import Colors from 'coeur/constants/color';
import Defaults from 'coeur/constants/default';
import Fonts from 'coeur/constants/font';
import Sizes from 'coeur/constants/size';

import LocalColors from 'utils/constants/color';

Colors.extend({
	...LocalColors,
})

Defaults.extend({
	DEBUG: process.env.DEBUG === 'true',
	STORE_KEY: 'yuna',
	MATCHBOX_MAX_ATTACHMENT: 3,
	ORDER_STATUSES: {
		UNPAID: 'Awaiting Payment',
		PAID: 'Paid',
		CANCELLED: 'Cancelled',
		PROCESSING: 'Processing',
		PACKING: 'Packing',
		ON_COURIER: 'Shipping',
		DELIVERED: 'Delivered',
		RETURNED: 'Returned',
		RETURN_REJECTED: 'Return Rejected',
		EXPIRED: 'Expired',
	},
})

Fonts.extend({
	lib: 'Libre Baskerville',
	geo: 'Geomanist',
	alv: 'Alvaro',
	axs: 'Axis',
})

Sizes.extend({
	app: {
		// eslint-disable-next-line no-nested-ternary
		// width: Sizes.screen.width >= 1920
		// 	? 1920
		// 	// eslint-disable-next-line no-nested-ternary
		// 	: Sizes.screen.width >= 1440
		// 		? 1440
		// 		// eslint-disable-next-line no-nested-ternary
		// 		: Sizes.screen.width >= 1280
		// 			? 1280
		// 			// eslint-disable-next-line no-nested-ternary
		// 			: Sizes.screen.width >= 1024
		// 				? 1024
		// 				: Sizes.screen.width >= 414
		// 					? 414
		// 					: Sizes.screen.width,
		width: Sizes.app.width,
		height: Sizes.app.height,
	},
	fluid(current, width = 1920) {
		return Math.floor(current * Sizes.app.width / width)
	},
})


/*
| Extending helpers
*/

import ErrorHelper from 'coeur/helpers/error';
import TimeHelper from 'coeur/helpers/time';

ErrorHelper
	.addCode('000', 'Connection timeout')
	.addCode('001', 'Connection reset')
	.addCode('002', 'Something went wrong')
	.addCode('003', 'Facebook connect rejected')
	.addCode('004', 'Missing email')
	.addCode('005', 'Email already registered')
	.addCode('006', 'Email and password do not match')
	.addCode('007', 'Account invalid')
	.addCode('008', 'There is blocker ahead')
	.addCode('009', 'Nonce not provided')
	.addCode('010', 'Detail not found')
	.addCode('011', 'User not verified')
	.addCode('012', 'Item invalid')
	.addCode('013', 'Passwords doesn\'t match')
	.addCode('031', 'You have logged out of session. Please login again')
	.addCode('032', 'Request overriden')

TimeHelper.extend({
	getRange(_start, _end, dateFormat = 'D', monthFormat = 'MMMM', yearFormat = 'YYYY') {
		const start = TimeHelper.moment(_start)
			, end = TimeHelper.moment(_end)
			, isSameYear = start.year() === end.year()
			, isSameMonth = start.month() === end.month()

		return isSameYear
			? isSameMonth
				&& `${start.date()} - ${end.date()} ${end.format(`${monthFormat} ${yearFormat}`)}`
				|| `${start.format(`${dateFormat} ${monthFormat}`)} - ${end.format(`${dateFormat} ${monthFormat} ${yearFormat}`)}`
			: `${start.format(`${dateFormat} ${monthFormat} ${yearFormat}`)} - ${end.format(`${dateFormat} ${monthFormat} ${yearFormat}`)}`
	},
})
