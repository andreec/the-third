import ManagerModel from 'coeur/models/manager'

// import MeService from 'app/services/me'

import UserRecord from 'app/records/user'


class MeManager extends ManagerModel {

	static displayName = 'me'

	constructor() {
		super(UserRecord, {
			additionalProperties: {
				isVerified		: false,

				// balance			: 0,
				// birthday		: '',
				email			: '',
				phone			: '',
				token			: '',

				isStyleProfileCompleted		: false,
				defaultAddressId			: null,
				// defaultPaymentChannelId		: null,
				// defaultPaymentMethodId		: null,
				addressIds					: [],
				batchIds					: [],
				mediaIds					: [],

				// locally managed
				cartItems					: [],
			},
			expiricy: 1000 * 60 * 60 * 24 * 7,
		})

		this.onLoginStateChange = undefined
	}

	// ------------------------------------
	// Login & Register
	// ------------------------------------

	// authenticate(token) {
	// 	if(token) {
	// 		return MeService.authenticate(token).then(response => {
	// 			return this.update(response)
	// 		})
	// 	} else {
	// 		return Promise.resolve(false)
	// 	}
	// }

	// ------------------------------------
	// Data manipulation
	// ------------------------------------

	// updateProfile(data) {
	// 	return MeService
	// 		.updateData({
	// 			token: this.state.token,
	// 			...data,
	// 		})
	// 		.then(response => {
	// 			return this.update(response)
	// 		})
	// }

	// ------------------------------------
	// Password manipulation
	// ------------------------------------

	// ------------------------------------
	// Send Email
	// ------------------------------------

	// ------------------------------------
	// CAART
	// ------------------------------------

	logout() {
		this.dispatch({
			type: this.constants.RESET,
		})

		// TODO: facebook logout

		this.onLoginStateChange &&
		this.onLoginStateChange()
	}
}

export default new MeManager()
