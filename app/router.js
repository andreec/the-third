import React from 'react';
import Loadable from 'react-loadable';
import LoaderPage from 'modules/pages/loader';
// import HomePage from 'modules/pages/signup'
// import LoginPage from 'modules/pages/login'
// import MatchboxPage from 'modules/pages/address'

import {
	BrowserRouter,
	HashRouter,
	Redirect,
	Route,
	Switch,
} from 'react-router-dom';

export default {
	BrowserRouter,
	HashRouter,
	Route({ onMaintenance }) {
		return onMaintenance ? (
			<Route component={Loadable({
				loader: () => import('modules/pages/maintenance'),
				loading: LoaderPage,
			})} />
		) : (
			<Switch>
				<Route exact path="/" component={ Loadable({
					loader: () => import('modules/pages/home').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<Route exact path="/registration" component={ Loadable({
					loader: () => import('modules/pages/registration').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<Route exact path="/checkout" component={ Loadable({
					loader: () => import('modules/pages/checkout').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<Route exact path="/maintenance" component={ Loadable({
					loader: () => import('modules/pages/maintenance').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<Route component={ Loadable({
					loader: () => import('modules/pages/four.o.four').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<Redirect exact from="/home" to="/" />
			</Switch>
		)
	},
}
