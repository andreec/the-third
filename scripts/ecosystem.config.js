module.exports = {
	apps: [{
		name: 'Weblique',
		script: './server/index.js',
		instances: 'max',
		exec_mode : 'cluster',
		env: {
			NODE_PATH: './',
		},
	}],
	deploy: {
		staging: {
			user: 'root',
			host: '139.162.45.94',
			ref: 'origin/release-1.5.7.1',
			repo: 'git@bitbucket.org:yuna-frontend/weblique.git',
			path: '/home/ubuntu/weblique',
			'post-deploy': './scripts/setenv staging && npm i && npm i coeur && npm run bundle && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
		production: {
			user: 'root',
			host: '139.162.47.153',
			ref: 'origin/release-1.5.7.1',
			repo: 'git@bitbucket.org:yuna-frontend/weblique.git',
			path: '/home/ubuntu/weblique',
			'post-deploy': './scripts/setenv production && npm i && npm i coeur && npm run bundle && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
	},
}
