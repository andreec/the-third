const fs = require('fs');
const readDir = require('fs-readdir-recursive');
const _ = require('lodash');

const ignoreRegex = /^[._]|^(style|config|view)/
	, indexJsRegex = /^index\.(js|jsx)$/i
	, pathIndexOrExtRegex = /(\/index\.[a-zA-Z]*$|.[a-zA-Z]*$)/
	, imageSetRegex = /@[0-9]x\./
	, ignoreExtension = ['jpg', 'png', 'gif', 'js', 'jsx', 'mp4']
	, ignoreFilter = function(indexDir, ignore = ignoreRegex, file, i, dir) {
		return !file.match(ignore) && (indexDir === dir ? !file.match(indexJsRegex) : true)
	}
	, capitalizeFirstLetter = function(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

export default function(INDEXES) {
	INDEXES.map(i => {
		const files = readDir(i.path, ignoreFilter.bind(this, i.path, i.ignore))
		let imports = ''
		let exports = ''

		_.uniqBy(files.map(f => {
			const folders = f.split('/').map(capitalizeFirstLetter)
				, rawFileName = folders.pop()
				, moduleName = folders.join('').split('.').map(capitalizeFirstLetter).join('').split('-').map(capitalizeFirstLetter).join('')
				, fileName = !!rawFileName.match(indexJsRegex) ? '' : rawFileName.split('.').filter(fN => ignoreExtension.indexOf(fN) === -1).map(capitalizeFirstLetter).join('').split('@')[0].split('-').map(capitalizeFirstLetter).join('')
				, pathName = !!f.match(imageSetRegex) ? f.replace(imageSetRegex, '.') : f
				, pathWithoutIndexOrExt = pathName.replace(pathIndexOrExtRegex, '')

			return {
				module: moduleName,
				file: fileName,
				key: moduleName + fileName + i.key,
				path: pathName,
				pathfile: pathWithoutIndexOrExt,
			}
		}), 'key').forEach(fC => {
			imports += `import ${fC.key} from './${fC.pathfile}'\n`
			exports += `	${fC.key},\n`
		})

		fs.writeFile(`${i.path}/index.js`, `${imports}\n\nexport {\n${exports}}\n`, (err) => {
			if(err) throw err;
		});
	})

	console.log('::gulp:index:: Done updating index.js');
}
