const fs = require('fs');
const readDir = require('fs-readdir-recursive');

const fileToRemoveRegex = /\.DS_Store/i


export default function() {
	const toBeRemoved = readDir('./', () => true)

	!toBeRemoved.filter(f => {
		return f.match(fileToRemoveRegex)
	}).map(f => {
		fs.unlink(f, (err) => {
			if(err) throw err;
			console.log(`::gulp:remove:dsstore:: ${f} was deleted`);
		});

		return true
	}).length && console.log('::gulp:remove:dsstore:: no .DS_Store to delete');
}
